case 60: opcode("open")
  /* open: name-addr name-len mode -- fd|-1 */
  {
    int mode = pop(sp);
    GET_NAME
    push(sp) = -1;
    if( name )
    {
        top(sp) = open( name, mode, 0 );
    }
    DROP_NAME
  }
  break;

case 61: opcode("close")
  /* close: fd -- status */
  {
    top(sp) = close(top(sp));
  }
  break;

case 62: opcode("read")
  /* read: fd addr len -- len|-1 */
  {
    g_cell len   = pop(sp);
    g_cell where = pop(sp);
    g_cell what  = top(sp);
    top(sp) = -1;
    unsigned char* buffer = (unsigned char*) forth_malloc( len );
    if( buffer )
    {
      top(sp) = val = read(what,buffer,len);
      if( val > 0 )
      {
        g_cell p;
        for( p = 0; p < val; p++ )
        {
          cpoke(where+p,buffer[p]);
        }
      }
    }
  }
  break;

case 63: opcode("write")
  /* write: fd addr len -- len|-1 */
  {
    g_cell len   = pop(sp);
    g_cell where = pop(sp);
    g_cell what  = top(sp);
    top(sp) = -1;
    unsigned char* buffer = (unsigned char*) forth_malloc( len );
    if( buffer )
    {
      g_cell p;
      for( p = 0; p < len; p++ )
      {
        buffer[p] = cpeek(where+p);
      }
      top(sp) = write(what,buffer,len);
    }
  }
  break;

