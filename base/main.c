/*
 * (c) 2003-2018 Stephane Alnet
 */
#include "base.h"
#include "loader.h"
#include "memory.h"

#ifdef MEMORY_BASE_CODE  /* Use in-memory image */
extern unsigned char memory_base_code[]      asm("_binary_memory_base_code_start");
extern unsigned char memory_base_code_end[]  asm("_binary_memory_base_code_end");
#endif

/* Note: due to the fact that run() will reset SP, RP and IP every time it is
         called, only the memory is kept as-is during file changes.
         Especially, it is not possible to pass values on either stacks from
         one file to another (this may be considered a feature), and every
         file starts within the context of a new "ABORT" (IP=IP_START).
*/

int main( int argc, char const* argv[] )
{
    f_memory p;
    initiate_mem(&p);

#ifdef MEMORY_BASE_CODE  /* Use in-memory image */
    unsigned memory_base_code_size = memory_base_code_end - memory_base_code;

    if( bootstrap_mem(&p,memory_base_code,memory_base_code_size) )
    {
      fprintf(stderr,"Unable to initialize.\n");
      return 2;
    }
#else
  #ifdef THE_BASE_CODE  /* Use static filename */
    /* Fill memory with the base Forth system. */
    if( bootstrap_file(&p,THE_BASE_CODE) )
    {
       fprintf(stderr,"Unable to initialize.\n");
       return 2;
    }
  #else  /* Binary bootstrap image filename is provided */
    argv++, argc--;
    if( argc < 1 )
    {
      fprintf(stderr,"Please provide binary boot image.\n");
      return 2;
    }
    /* Fill memory with the base Forth system. */
    if( bootstrap_file(&p,*argv) )
    {
       fprintf(stderr,"Unable to initialize.\n");
       return 2;
    }
  #endif
#endif
    /* Process argument(s) */
    int code = 0;
    if( argc > 1 )
    {
        /* One argument or more indicates Forth source code filenames. */
        /* As a special case, dash '-' is used to mean standard input. */
        int argn;
        for( argn = 1; argn < argc; argn++ )
        {
            char const* filename = argv[argn];
            FILE* new_input_stream =
                (filename[0] == '-' && filename[1] == '\0') 
                   ? stdin : fopen(filename,"r");
            if(new_input_stream)
            {
                fprintf(stderr,"Running \"%s\".\n",filename);
                input_stream = new_input_stream;
                /* Start the virtual machine */
                code = run(&p,0);
                fclose(input_stream);
                fprintf(stderr,"Done \"%s\".\n",filename);
                if(code) {
                  return code;
                }
            }
            else
            {
                fprintf(stderr,"An error occurred while opening \"%s\".\n",
                               filename);
                return 1;
            }
        }
    }
    else
    {
        /* If no arguments are given, simply start parsing from stdin. */
#ifdef THE_BASE_RUNTIME
        /* Load the interactive engine. */
        if( startup(&p,THE_BASE_RUNTIME) )
        {
          fprintf(stderr,"Unable to initialize.\n");
          return 2;
        }
#endif
        input_stream = stdin;
        /* Start the virtual machine */
        code = run(&p,1);
        fclose(input_stream);
    }

    return code;
}

