/*
 * (c) 2003-2008 Stephane Alnet
 */

#undef REQUIRE_BASE_RUNTIME

#include <stdio.h>

/* Type definitions */

#include "loader.h"
#include "memory.h"

/* Called once at startup time to initialize the system memory
  with the base Forth machine. */

/* Load binary bootstrap Forth code from file. */
int bootstrap_file( f_memory* p, char const* base_code )
{
    unsigned char* cmem;

    fprintf(stderr,"Using bootstrap file %s\n", base_code);

    /* Open bootstrap binary file. */
    FILE* code_base_file = fopen( base_code, "rb" );
    if( ! code_base_file  )
    {
      fprintf(stderr,"Unable to open %s\n", base_code);
      return 1;
    }
    /* Obtain size of the bootstrap block. */
    if( fseek( code_base_file, 0, SEEK_END ) )
      return 1;
    int sizeof_code_base = ftell( code_base_file );
    if( sizeof_code_base <= 0 || sizeof_code_base >= MEM_BUCKET_SIZE )
    {
      fprintf(stderr,"File %s has an invalid size (%d bytes).\n",
                     base_code, sizeof_code_base);
      return 1;
    }
    /* Read bootstrap block at once. */
    if( fseek( code_base_file, 0, SEEK_SET ) )
      return 1;
    cmem = forth_malloc(sizeof_code_base);
    if(!cmem) {
      fprintf(stderr,"Could not allocate memory buffer for bootstrap file\n");
      return 1;
    }

    int nread = fread( cmem, 1, sizeof_code_base, code_base_file );
    if( nread != sizeof_code_base )
    {
      fprintf(stderr,"File %s is %d bytes long but only %d bytes were read.\n",
                     base_code, sizeof_code_base, nread);
      return 1;
    }
    fclose( code_base_file );
    fprintf(stderr,"Loaded %d bytes\n", nread);
    return bootstrap_mem(p,cmem,sizeof_code_base);
}

/* Load binary bootstrap Forth code from memory. */
int bootstrap_mem( f_memory* p, unsigned char const* base_code, size_t base_size )
{
    g_cell* cell_mem;
    g_char* char_mem;

    if( base_size < 2*sizeof(g_cell) )
    {
      fprintf(stderr,"Bootstrap is %lu bytes long, which is too short.\n",
                     base_size);
      return 1;
    }

    g_cell* base_code_cell = (g_cell*)base_code;

    /* Initialize Forth machine memory. */
    initiate_mem(p);
    cell_mem = cell_page_zero(p);
    char_mem = char_page_zero(p);

    /* The first two cells are the block sizes. */
    g_cell cell_count = base_code_cell[0];
    g_cell char_count = base_code_cell[1];

    if( base_size != sizeof(g_cell)*(cell_count+2) + sizeof(g_char)*(char_count) )
    {
      fprintf(stderr,"Bootstrap is %lu bytes long, which doesn't match " DISPLAY_CELL " cells and " DISPLAY_CELL " chars.\n",
                     base_size,cell_count,char_count);
      return 1;
    }

    memcpy(cell_mem,base_code+sizeof(g_cell)*2,sizeof(g_cell)*cell_count);
    memcpy(char_mem,base_code+sizeof(g_cell)*(cell_count+2),sizeof(g_char)*char_count);

    fprintf(stderr,"Loaded " DISPLAY_CELL " cells, " DISPLAY_CELL " chars\n",cell_count,char_count);

    return 0;
}

/* Compiles (interactive) startup source code. */
int startup( f_memory* p, char const* runtime )
{
    int code = -1;
    input_stream = fopen( runtime, "r" );
    if( input_stream )
    {
      code = run(p,0);
      fclose(input_stream);
    }
#ifdef REQUIRE_BASE_RUNTIME
    else
    {
      fprintf(stderr,"An error occurred while opening \"%s\".\n",
                     runtime);
      code = 1;
    }
#endif /* REQUIRE_BASE_RUNTIME */
    return code;
}
