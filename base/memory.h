#ifndef BASE_MEMORY_H
#define BASE_MEMORY_H

#include "base.h"

void initiate_mem( f_memory* p );
g_char* char_page_zero( f_memory* p );
g_cell* cell_page_zero( f_memory* p );

g_cell __peek( f_memory* p, g_cell a );
g_char __cpeek( f_memory* p, g_cell a );

void __poke( f_memory*p, g_cell a, g_cell v );
void __cpoke( f_memory* p, g_cell a, g_char v );

/* GC or not? */

// #undef  FORTH_HAS_GC

#ifdef FORTH_HAS_GC
  /* boehmgc */
  #include <gc.h>
  #define forth_malloc(sz) GC_MALLOC(sz)
  #define forth_free(p)
#else
  #include <stdlib.h> /* malloc() */
  #define forth_malloc(sz) malloc(sz)
  #define forth_free(p)    free(p)
#endif


#endif
