case 70: /* socket */
  /* socket: AF_* SOCK_* proto_num -- fd|-1 */
  {
      g_cell sock_type = Spop;
      g_cell af_family = Spop;
      sp_top = socket(af_family,sock_type,sp_top);
  }
  break;

case 71: /* listen */
  /* listen: fd-stream backlog -- status */
  {
      g_cell fd = Spop;
      sp_top = listen(fd,sp_top);
  }
  break;

case 72: /* bind */
  /* bind: fd .. */
  break;

case 73: /* accept */
  /* accept: fd-stream .. */
  break;

case 74: /* connect */
  /* connect: fd name -- status */
  {
    GET_NAME
    push(sp) = -1;
    if( name )
    {
              // XXX
    }

    DROP_NAME
  }
  break;
