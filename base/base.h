/*
 * (c) 2003-2008 Stephane Alnet
 */
#ifndef __BASE_H
#define __BASE_H

#include "params.h"

/* Standard I/O. */
#include <stdio.h>

/* Exposed state. */
FILE* input_stream;

/* Memory initialization (used by the bootstrap loader). */

typedef struct f_memory {
  g_cell* cell_mem_array[MEM_ARRAY_SIZE];          /* Indirect Memory pointers */
  g_char* char_mem_array[MEM_ARRAY_SIZE];          /* Indirect Memory pointers */
} f_memory;

/* Main loop, single thread. */
int run( f_memory*, short interactive );

#endif /* __BASE_H */
