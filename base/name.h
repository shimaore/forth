#ifndef BASE_NAME_H
#define BASE_NAME_H

#define GET_NAME                          \
  g_cell name_length = pop(sp);           \
  g_cell name_addr   = pop(sp);           \
                                          \
  char* name = (char*) forth_malloc( name_length+1 ); \
  if(name)                                \
  {                                       \
    int i;                                \
    for( i = 0; i < name_length; i++ )    \
    {                                     \
      name[i] = cpeek(name_addr+i);       \
    }                                     \
    name[i] = '\0';                       \
  }
#define DROP_NAME forth_free(name);

#endif