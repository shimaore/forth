case 100: opcode(">f")
  push(fp) = pop(sp);
  break;
case 101: opcode("f>")
  push(sp) = pop(fp);
  break;
case 102: opcode("f+")
  f_val = pop(fp);
  top(fp) += f_val;
  break;
case 103: opcode("fnegate")
  top(fp) = -top(fp);
  break;
case 104: opcode("f*")
  f_val = pop(fp);
  top(fp) *= f_val;
  break;
case 105: opcode("f/")
  f_val = pop(fp);
  top(fp) /= f_val;
  break;
case 106: opcode("floor")
  top(fp) = floor(top(fp));
  break;
case 107: opcode("fmod")
  f_val = pop(fp);
  top(fp) = fmod(top(fp),f_val);
  break;
case 108: opcode("fdup")
  f_val = top(fp);
  push(fp) = f_val;
  break;
case 109: opcode("fdrop")
  (void)pop(fp);
  break;
case 110: opcode("fswap")
  f_val = top(fp);
  top(fp) = top2(fp);
  top2(fp) = f_val;
  break;
// Missing: f!, f@, falloc
