/* Type definitions */

#include "memory.h"

/* ------ Memory Management ------ */

#define page(a)    (a >> MEM_BUCKET_BITS)
#define in_page(a) (a &  MEM_BUCKET_MASK)

inline void allocate_cell_page( f_memory* p, g_cell a )
{
  p->cell_mem_array[page(a)] =
    (g_cell*)forth_malloc( MEM_BUCKET_SIZE * sizeof(g_cell) );
}

inline void allocate_char_page( f_memory* p, g_cell a )
{
  p->char_mem_array[page(a)] =
    (g_char*)forth_malloc( MEM_BUCKET_SIZE * sizeof(g_char) );
}

void initiate_mem( f_memory* p )
{
  #ifdef FORTH_HAS_GC
    GC_INIT();
  #endif
  int i;
  for( i = 0; i < MEM_ARRAY_SIZE; i++ )
  {
    p->cell_mem_array[i] = NULL;
    p->char_mem_array[i] = NULL;
  }
}

g_cell* cell_page_zero( f_memory* p )
{
  if(!p->cell_mem_array[0])
    allocate_cell_page(p,0);
  return p->cell_mem_array[0];
}

g_char* char_page_zero( f_memory* p )
{
  if(!p->char_mem_array[0])
    allocate_char_page(p,0);
  return p->char_mem_array[0];
}

inline g_cell __peek( f_memory* p, g_cell a )
{
  g_cell pg = page(a);
  return (pg < MEM_ARRAY_SIZE && p->cell_mem_array[pg])
    ? p->cell_mem_array[pg][in_page(a)]
    : 0;
}

inline g_char __cpeek( f_memory* p, g_cell a )
{
  g_cell pg = page(a);
  return (pg < MEM_ARRAY_SIZE && p->char_mem_array[pg])
    ? p->char_mem_array[pg][in_page(a)]
    : 0;
}

/* Note: with the new architecture, the poke|cpoke are not guaranteed
 * to be successful. However they are guaranteed to finish.
 */

void __poke( f_memory*p, g_cell a, g_cell v )
{
  g_cell pg = page(a);
  if (pg < MEM_ARRAY_SIZE) {
    if(!p->cell_mem_array[pg])
      allocate_cell_page(p,a);
    if(p->cell_mem_array[pg])
      p->cell_mem_array[pg][in_page(a)] = v;
  }
}

void __cpoke( f_memory* p, g_cell a, g_char v )
{
  g_cell pg = page(a);
  if (pg < MEM_ARRAY_SIZE) {
    if(!p->char_mem_array[pg])
      allocate_char_page(p,a);
    if(p->char_mem_array[pg])
      p->char_mem_array[pg][in_page(a)] = v;
  }
}
