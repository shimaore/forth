#ifndef __params_h
#define __params_h

/*
 * Edit this file to specify the different paramateres of the Forth
 * machine.
 *
 * A g_char (Forth CHAR) is the smallest adressable unit in memory.
 * A g_cell (Forth CELL) is used to store memory addresses.
 *
 */

#define ONE_CELL 1

/* MAX_STK: Depth of the stacks (in CELLs). */
/* Note: the SP and RP stacks only store CELLs. */
#define SP_MAX_STK    1024
#define RP_MAX_STK    1024

#ifdef FORTH_HAS_FLOAT
#define FP_MAX_STK    1024
  typedef double         g_float;
#endif

/* System parameters: choose your system model here. */

#ifdef SYSTEM_TYPE_32
  /* Standard 32-bit system, CELL = uint, CHAR = uchar */
  typedef unsigned int   g_cell;
  typedef unsigned char  g_char;
  #define DISPLAY_CELL_HEX    "0x%08x"
  #define DISPLAY_CELL        "%u"
  #define ZERO                ((g_cell)0)
  #define ONE                 ((g_cell)1)
#endif

#ifdef SYSTEM_TYPE_24
typedef __uint32_t    g_cell;
typedef __uint8_t     g_char;
#define DISPLAY_CELL_HEX      "%06x"
#define DISPLAY_CELL          "%ud"
  #define ZERO                ((g_cell)0)
  #define ONE                 ((g_cell)1)

enum {
  ADDR_FLASH = 0x800000,
  ADDR_RAM   = 0x400000
};

#define ONE_CELL 3
#define COMMON_MEMORY
#define USE_FLASH
#endif

#ifdef SYSTEM_TYPE_64
  /*  64-bit system */
  typedef unsigned long long  g_cell;
  typedef unsigned char       g_char;
  #define DISPLAY_CELL_HEX    "0x%016llx"
  #define DISPLAY_CELL        "%llu"
  #define ZERO                ((g_cell)0L)
  #define ONE                 ((g_cell)1L)
#endif

#ifdef SYSTEM_TYPE_16
  /* Standard 16-bit system, CELL = ushort, CHAR = uchar */
  typedef unsigned short g_cell;
  typedef unsigned char  g_char;
  #define DISPLAY_CELL_HEX    "0x%04x"
  #define DISPLAY_CELL        "%u"
  #define ZERO                ((g_cell)0)
  #define ONE                 ((g_cell)1)
#endif

#if SYSTEM_TYPE_32_UNICODE
  /* 32-bit system with Unicode, CELL = uint, CHAR = ushort */
  typedef unsigned int   g_cell;
  typedef unsigned short g_char;
  #define DISPLAY_CELL_HEX    "%08x"
  #define DISPLAY_CELL        "%u"
  #define ZERO                ((g_cell)0)
  #define ONE                 ((g_cell)1)
#endif



/* MEM_BUCKET_BITS: Defines the size of a single memory buffer (or page).
 * MEM_ARRAY_BITS:  Defines the number of memory buffers (or pages).
 */
enum { MEM_BUCKET_BITS = 16 };

#ifdef SYSTEM_TYPE_64
enum { MEM_ARRAY_BITS = 16 };
#else
enum { MEM_ARRAY_BITS = 8*sizeof(g_cell)-MEM_BUCKET_BITS };
#endif

/* MEM_BUCKET: Size of a single memory buffer (in CHARs). */
#define MEM_BUCKET_SIZE    (ONE<<(g_cell)MEM_BUCKET_BITS)
#define MEM_BUCKET_MASK    (MEM_BUCKET_SIZE-ONE)
#define MEM_ARRAY_SIZE     (ONE<<(g_cell)MEM_ARRAY_BITS)


/* Other (computed) definitions. No user serviceable parts within. */

/* g_true:   the TRUE value (all bits ones)
   g_false:  the FALSE value (all bits zeroes)
   neg_mask: mask used to find if a CELL is a positive or a negative value.
*/

#define g_true   ((g_cell)(~ZERO))
#define g_false  ((g_cell)ZERO)

#define neg_mask (ONE << (g_cell)(8*sizeof(g_cell)-1))

#define IP_START      (256)
#define is_opcode(op) (op < 256)
#define map_opcode(op)    (op)
#define unmap_opcode(op)  (op)

#endif /* __params_h */

