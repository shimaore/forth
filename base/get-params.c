/*
 * (c) 2003-2008 Stephane Alnet
 */
#include <stdio.h>
#include "params.h"

int main( int argc, char const* argv[] )
{
    printf("\\ Automatically generated. Re-run %s to update.\n\
.inline true     " DISPLAY_CELL "\n\
.inline neg_mask " DISPLAY_CELL "\n\
",
           argv[0],
           (g_cell)g_true,
           (g_cell)neg_mask
           );

    return 0;
}

