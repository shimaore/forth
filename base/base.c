/*
 * (c) 2003-2009 Stephane Alnet
 */

#define FORTH_HAS_FILE_IO
#undef  FORTH_HAS_NETWORKING

// #undef  FORTH_FORCE_DEBUG
// #define FORTH_FORCE_STACK_CHECK
// #undef  FORTH_DEBUG_STATE
// #undef  FORTH_DEBUG_OPCODES   /* always on, if enabled */
// #undef  FORTH_STACK_CHECK     /* needs to be enabled using forth_stack_check! */

/* End of parameters */

#ifdef FORTH_FORCE_DEBUG
#define FORTH_DEBUG_STATE
#endif

#ifdef FORTH_FORCE_STACK_CHECK
#define FORTH_STACK_CHECK
#endif

#ifdef FORTH_DEBUG_STATE
#define FORTH_DEBUG_OPCODES
#define FORTH_STACK_CHECK
#endif

/* Type definitions */

#include "base.h"
#include "memory.h"

#ifdef FORTH_HAS_FILE_IO
   #include "fileio.h"
#endif /* FORTH_HAS_FILE_IO */
#ifdef FORTH_HAS_NETWORKING
   #include "networking.h"
#endif /* FORTH_HAS_NETWORKING */
#ifndef FORTH_NO_CMOVE
   #include <string.h>
#endif
#ifdef FORTH_HAS_FLOAT
   #include <math.h>
#endif

#ifdef FORTH_DEBUG_OPCODES
  #ifdef FORTH_DEBUG_STATE
   #define opcode(o)  if (state->forth_debug_state) fprintf(stderr,"Exec %s\n",o);
  #else
   #define opcode(o)  fprintf(stderr,"Exec %s\n",o);
  #endif
#else
   #define opcode(o)
#endif

/* Standard I/O. */

#include <stdio.h>  /* FILE*    */

FILE* input_stream;

/* -------- Model for a standard machine. ---------- */

typedef struct thread_state {

  int    running;
  g_cell ip;

  f_memory* p;

#ifdef FORTH_STACK_CHECK
  short forth_stack_check;
#endif
#ifdef FORTH_DEBUG_STATE
  short forth_debug_state;
#endif

  short forth_interactive;

  g_cell *sp;
  g_cell sp_start[SP_MAX_STK];                /* Data Stack */

  g_cell *rp;
  g_cell rp_start[RP_MAX_STK];                /* Return Stack */

#ifdef FORTH_HAS_FLOAT
  g_float *fp;
  g_float fp_start[FP_MAX_STK];               /* Float Stack */
#endif

} thread_state;

inline
void initialize_thread( thread_state* t, f_memory* p, short interactive )
{
    t->ip = IP_START;
    /* The stacks start at the end and grow downward */
    t->sp = t->sp_start+SP_MAX_STK;
    t->rp = t->rp_start+RP_MAX_STK;
#ifdef FORTH_HAS_FLOAT
    t->fp = t->fp_start+FP_MAX_STK;
#endif
    t->running = 1;
    t->p = p;
    t->forth_interactive = interactive;
}

void run_one_step( thread_state* state )
{

        #define sp   (state->sp)
        #define rp   (state->rp)
        #define ip   (state->ip)
        #define peek(a) __peek(state->p,a)
        #define cpeek(a) __cpeek(state->p,a)
        #define poke(a,x) __poke(state->p,a,x)
        #define cpoke(a,x) __cpoke(state->p,a,x)

        #define top(s)    *(s)
        #define top2(s)   *(s+1)
        #define pop(s)    *(s++)
        #define push(s)   *(--s)

        g_cell op, val;
#ifdef FORTH_HAS_FLOAT
        #define fp    (state->fp)
        g_float f_val;
#endif

        op = peek(ip);

#ifdef FORTH_DEBUG_OPCODES
#ifdef FORTH_DEBUG_STATE
        if (state->forth_debug_state)
#endif
        fprintf(stderr,"Found opcode " DISPLAY_CELL_HEX " at IP " DISPLAY_CELL_HEX "\n",op,ip);
#endif /* FORTH_DEBUG_OPCODES */

#ifdef FORTH_STACK_CHECK
        if(state->forth_stack_check)
        {
          if( sp > state->sp_start+SP_MAX_STK )
            { fprintf(stderr,"%s underflow at " DISPLAY_CELL_HEX "\n","sp",ip); state->running = -1; return; }
          if( rp > state->rp_start+RP_MAX_STK )
            { fprintf(stderr,"%s underflow at " DISPLAY_CELL_HEX "\n","rp",ip); state->running = -1; return; }
          if( sp <= state->sp_start )
            { fprintf(stderr,"%s overflow at " DISPLAY_CELL_HEX "\n","sp",ip); state->running = -1; return; }
          if( rp <= state->rp_start )
            { fprintf(stderr,"%s overflow at " DISPLAY_CELL_HEX "\n","rp",ip); state->running = -1; return; }
#ifdef FORTH_HAS_FLOAT
          if( fp > state->fp_start+FP_MAX_STK )
            { fprintf(stderr,"%s underflow at " DISPLAY_CELL_HEX "\n","fp",ip); state->running = -1; return; }
          if( fp <= state->fp_start )
            { fprintf(stderr,"%s overflow at " DISPLAY_CELL_HEX "\n","fp",ip); state->running = -1; return; }
#endif /* FORTH_HAS_FLOAT */
        }
#endif /* FORTH_STACK_CHECK */

#ifdef FORTH_DEBUG_STATE
        if(state->forth_debug_state)
        {
          g_cell* tmp;
          fprintf(stderr,"%s: ","sp");
          for( tmp = sp; tmp < state->sp_start+SP_MAX_STK; tmp++ )
          {
            fprintf(stderr," " DISPLAY_CELL_HEX,*tmp);
          }
          fprintf(stderr,"\n");

          fprintf(stderr,"%s: ","rp");
          for( tmp = rp; tmp < state->rp_start+RP_MAX_STK; tmp++ )
          {
            fprintf(stderr," " DISPLAY_CELL_HEX,*tmp);
          }
          fprintf(stderr,"\n");
          fprintf(stderr,"ip: " DISPLAY_CELL_HEX " .. ",ip);
        }
#endif /* FORTH_DEBUG_STATE */

        ip ++;

        if( is_opcode(op) )
        {
            switch( map_opcode(op) )
            {
            /* Note: This is not the smallest core possible.
             *       BRANCH, 0BRANCH and LIT, for example, can be
             *       implemented in FORTH itself.
             *       However the performances tend to be unacceptable
             *       because these are used very often.
             */

            /*
             * Do not change the format of the 'case' lines, they are used to
             * automatically generate opcodes.f and opcodes.t.
             */

            case  0: opcode("exit")
              ip     = pop(rp);
              break;

            case  1: opcode(">r")
              push(rp) = pop(sp);
              break;
            case  2: opcode("r@")
              push(sp) = top(rp);
              break;
            case  3: opcode("r>")
              push(sp) = pop(rp);
              break;

            case  4: opcode("dup")
              val = top(sp);
              push(sp) = val;
              break;
            case  5: opcode("drop")
              (void)pop(sp);
              break;
            case  6: opcode("rdrop")
              (void)pop(rp);
              break;
            case  7: opcode("nip")
              val = pop(sp);
              top(sp) = val;
            break;

            // take 2, return 2
            /* The following is a complicated way to implement "add with carry"
             * in C. Semantically this can also be thought as returning the
             * (double) sum of two unsigned CELLs.
             */
            case  8: opcode("+c")
              val  = top(sp) + top2(sp);
              top(sp) = ((val < top(sp)) || (val < top2(sp))) ? 1 : 0;
              top2(sp) = val;
              break;
            case  9: opcode("swap")
              val = top(sp);
              top(sp) = top2(sp);
              top2(sp) = val;
              break;

            // take 2, return 3
            case 10: opcode("over")
              val = top2(sp);
              push(sp) = val;
              break;

            // take 2, consumes 1, return 1
            // not required
            case 11: opcode("u<")
              val = pop(sp);
              top(sp) = (top(sp) < val ? g_true : g_false);
              break;

            case 12: opcode("+")
              val = pop(sp);
              top(sp) += val;
              break;
            case 13: opcode("and")
              val = pop(sp);
              top(sp) &= val;
              break;
            case 14: opcode("or")
              val = pop(sp);
              top(sp) |= val;
              break;
            // not required
            case 15: opcode("xor")
              val = pop(sp);
              top(sp) ^= val;
              break;

            // in-place (take 1, return 1)
            /* These operations are the ones that most benefit from having
               sp_top as a register.
            */
            case 16: opcode("1+")
              top(sp) += 1;
              break;
            case 17: opcode("0=")
              top(sp) = (top(sp) == 0 ? g_true : g_false);
              break;
            case 18: opcode("negate")
              top(sp) = -(top(sp));
              break;
            case 19: opcode("invert")
              top(sp) = ~(top(sp));
              break;
            case 20: opcode("-")
              val = pop(sp);
              top(sp) -= val;
              break;

            case 21: opcode("2/")
              top(sp) >>= 1;
              break;

            case 22: opcode("1-")
              top(sp) -= 1;
              break;

            case 23: // opcode("execute")
              push(rp) = ip;
              ip = pop(sp);
              break;

            case 24: opcode("c@")
              top(sp) = cpeek(top(sp));
              break;
            case 25: opcode("@")
              top(sp) = peek(top(sp));
              break;

            // take 2, consumes 2, return 0
            case 26: opcode("c!")
              #ifdef FORTH_DEBUG_OPCODES
                #ifdef FORTH_DEBUG_STATE
                if (state->forth_debug_state)
                #endif
                printf("%c %d (0x%04x) c!\n",top2(sp),top(sp),top(sp));
              #endif
              val = pop(sp);
              cpoke(val,pop(sp));
              break;
            case 27: opcode("!")
              #ifdef FORTH_DEBUG_OPCODES
                #ifdef FORTH_DEBUG_STATE
                if (state->forth_debug_state)
                #endif
                printf("%d %d (0x%04x) !\n",top2(sp),top(sp),top(sp));
              #endif
              val = pop(sp);
              poke(val,pop(sp));
              break;

            // take 0, return 1
            case 28: opcode("lit")
              #ifdef FORTH_DEBUG_OPCODES
              #ifdef FORTH_DEBUG_STATE
              if (state->forth_debug_state)
              #endif
              fprintf(stderr,"     LIT " DISPLAY_CELL "\n",peek(ip));
              #endif
              push(sp) = peek(ip);
              ip ++;
              break;

            // take 1, return 0
            case 29: opcode("0branch")
              ip = (pop(sp) == 0)
                    ? peek(ip)
                    : ip+1;
              break;

            // take 0, return 0
            case 30: opcode("branch")
              ip = peek(ip);
              break;

            case 31: opcode("bye")
              state->running = 0;
              break;
            case 32: opcode("rp!")
              rp = state->rp_start+RP_MAX_STK;
              break;
            case 33: opcode("sp!")
              sp = state->sp_start+SP_MAX_STK;
              break;

            // take 0, return 1 (I/O)
            case 34: opcode("get-key")
              val = feof(input_stream)
                          ? g_false
                          : getc(input_stream);
              // putchar(val);
              push(sp) = val;
              break;

            // take 1, return 0 (I/O)
            case 35: opcode("emit")
              putchar(pop(sp));
              break;

            // take 0, return 1 (I/O)
            case 36: opcode("eof")
              push(sp) = feof(input_stream)
                          ? g_true
                          : g_false;
              break;

            case 37: opcode("?interactive")
              push(sp) = state->forth_interactive;
              break;

            #ifndef FORTH_NO_MULDIV
            case 38: opcode("u*")
              // This works because g_cell is always unsigned.
              val = pop(sp);
              top(sp) *= val;
              break;

            case 39: opcode("u/mod")
              // This works because g_cell is always unsigned.
              val = top(sp);
              top(sp) = top2(sp)/val;
              top2(sp) %= val;
              break;
            #endif

            #ifndef FORTH_NO_CMOVE
            case 40: opcode("cmove")
              {
                g_cell len = pop(sp);
                g_cell dst = pop(sp);
                g_cell src = pop(sp);
                while(len--) { cpoke(dst++,cpeek(src++)); }
              }
              break;
            #endif /* FORTH_NO_CMOVE */

            case 41: opcode("move")
              {
                g_cell len = pop(sp);
                g_cell dst = pop(sp);
                g_cell src = pop(sp);
                while(len--) { poke(dst++,peek(src++)); }
              }
              break;

            case 42: opcode("true")
              push(sp) = g_true;
              break;

            case 43: opcode("false")
              push(sp) = g_false;
              break;

            case 44: opcode("2*")
              top(sp) *= 2;
              break;

            case 45: opcode("2dup")
              val = top2(sp);
              push(sp) = val;
              val = top2(sp);
              push(sp) = val;
              break;

            case 46: opcode("2drop");
              (void)pop(sp);
              (void)pop(sp);
              break;

            case 47: opcode("+!");
              val = pop(sp);
              poke(val,peek(val)+pop(sp));
              break;

            case 48: opcode("0<");
              val = top(sp);
              top(sp) = (val & neg_mask) ? g_true : g_false;
              break;

            case 49: opcode("neg_mask");
              push(sp) = neg_mask;
              break;

            #ifdef FORTH_HAS_FILE_IO
              #include "fileio.c"
            #endif /* FORTH_HAS_FILE_IO */

            #ifdef FORTH_HAS_NETWORKING
              #include "networking.c"
            #endif /* FORTH_HAS_NETWORKING */

            #ifdef FORTH_HAS_FLOAT
              #include "float.c"
            #endif

            case 99: opcode("emit_cell")
              val = pop(sp);
              fwrite(&val,sizeof(g_cell),1,stdout);
              break;

            default:
                /* Invalid opcode. */
              #ifdef FORTH_DEBUG_OPCODES
                #ifdef FORTH_DEBUG_STATE
                if (state->forth_debug_state)
                #endif
                fprintf(stderr,"Invalid opcode " DISPLAY_CELL_HEX " at " DISPLAY_CELL_HEX "\n",op,ip-1);
              #endif
              break;
            }
        }
        else /* sub-call */
        {
            #ifdef FORTH_DEBUG_OPCODES
              #ifdef FORTH_DEBUG_STATE
              if (state->forth_debug_state)
              #endif
              fprintf(stderr,"call sub " DISPLAY_CELL_HEX "\n",op);
            #endif

            push(rp) = ip;
            ip = op;
        }
}

/* Main loop for a single-threaded system. */
int run( f_memory* p, short interactive )
{
    thread_state t;
    initialize_thread(&t,p,interactive);
    #ifdef FORTH_FORCE_DEBUG
    t.forth_debug_state = 1;
    #endif
    #ifdef FORTH_FORCE_STACK_CHECK
    t.forth_stack_check = 1;
    #endif
    /* run */
    while(t.running > 0)
    {
        run_one_step(&t);
    } /* while */
    return t.running;
} /* run() */

/* --- End of file base.c --- */
