: literal [literal] ; immediate

: 0  [  ( decimal )  0 ] literal ;
: 1  [  ( decimal )  1 ] literal ;
: 2  [  ( decimal )  2 ] literal ;
: 3  [  ( decimal )  3 ] literal ;

: -1     [ 1 negate ] literal ;
\ : false  [ 0 dup xor ] literal ;
\ : true   [ false invert ] literal ;

: u>     swap u< ;
: <=     > invert ;
: >=     < invert ;
: 0>     0 > ;

: +-   0< if negate then ;
: abs  dup +- ;

: constant  create , does> @ ;
: variable  create 0 , does> ;

: '    (find) nip ;
: [']  ' [literal] ; immediate

: ?execute ?dup if execute then ;
