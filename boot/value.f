\ VALUE has the same definition as CONSTANT
: value create , does> @ ;
: to ' >body ! ;

\ FUNCTION executes instead of retrieving
: function create 0 , does> @ ?execute ;
\ use TO to modify the function:   xt TO function-name
