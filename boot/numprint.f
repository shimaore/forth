\ Number printing

: digit>
  9 over < if 7 + then
  [char] 0 +
  ;

variable hld
: <#    pad hld ! ;
: hold  -1 ( chars ) hld +! hld @ c! ;
: #>    drop hld @ pad over - ;

: #     base @ u/mod swap digit> hold ;

: #s    begin # dup 0= until ;
: sign  0< if [char] - hold then ;
: u.    <# #s #> type space ;
: .     dup >r abs <# #s r> sign #> type space ;

