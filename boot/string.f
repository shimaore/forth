\ Defined in bootstrap
: type
  begin
    ?dup
  while
    over c@ emit
    1- swap char+ swap
  repeat
  drop ;

\ XXX should error if WORD returns 0
: .(  [char] ) word count type ; immediate

: c"
  postpone ahead >r \ jump forward, resolved by `then`
  [char] " word
  \ should error if 0
  word,
  r> postpone then
  [literal]
  ; immediate

: s" postpone c" postpone count ; immediate
: ." postpone s" postpone type ; immediate

: cr     carriage emit newline emit ;
: space  bl emit ;

: spaces  begin dup 0> while 1- space repeat drop ;

: c,      char-here 1 ( chars ) callot c! ;
