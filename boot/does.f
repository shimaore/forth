: does>
  postpone (does>)   \ last instruction of the defining word
  postpone r>        \ first instruction of the defined word
  ; immediate

\ >BODY
\ returns a pointer to the data section of a word created by CREATE
: >body  ( xt -- data-addr )  cell+ ;
