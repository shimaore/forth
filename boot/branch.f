
\ (BRANCH,LIT) MARK .. FORWARD (absolute)
: mark     here 0 , ;
: forward  here swap ! ;

\ HERE .. (BRANCH,LIT) BACK (absolute)
: back   , ;

\ AHEAD (not executed) .. THEN (continues here)
\ (i.e. skip/jump ahead)
: ahead  postpone branch mark ; immediate

: if     postpone 0branch mark ; immediate
: then   forward ; immediate
: else   >r postpone ahead r> postpone then ; immediate

: begin  here ; immediate
: again  postpone branch back ; immediate
: until  postpone 0branch back ; immediate
: while  >r postpone if r> ; immediate
: repeat postpone again postpone then ; immediate

: recurse postpone branch pfa @ compile, ; immediate
