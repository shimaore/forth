\ :NONAME creates an anonymous sub.

: :noname  ( -- xt )
  0 nfa !  \ no name, no NFA
  pfa!     \ save the location in PFA so that (e.g.) RECURSE works
  here     \ puts the xt on the stack
  ]        \ switch to compilation mode
  ;
