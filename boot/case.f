: resolve-forward
  begin
    ?dup
  while
    dup @ swap forward
  repeat
  ;

( case-sys :: endof-here )
: case ( C: -- case-sys ) ( -- )
  0 ; immediate

: (of) ( x1 x2 -- ftrue | x1 ffalse )
  over =
  dup if nip then
  ;

( of-sys :: here )
: of ( C: -- of-sys ) ( x1 x2 -- | x1 )
  postpone (of)
  postpone if
  ; immediate

: endof  ( C: case-sys1 of-sys -- case-sys2 )
  swap
  ( S: of-here endof-here -- )
  postpone branch
  here >r
  , ( Store the last endof-pointer in the linked list )
  postpone then
  r> ( -- case-sys )
  ; immediate

: endcase ( C: case-sys -- ) ( x -- )
  postpone drop
  resolve-forward
  ; immediate
