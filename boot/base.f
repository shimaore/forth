\ base.f: Provides BASE and associated tools.

\ bootstrap.f only support decimal
variable base
: decimal ( decimal ) 10 base ! ;
: hex     ( decimal ) 16 base ! ;
: bin                  2 base ! ;
\ Initialize BASE
decimal

\ (BASE) : u -- u true | false
: (base) dup base @ <  dup invert if nip then ;

\ >DIGIT : c -- u true | false
: >digit
  [char] 0 -
  dup 0<
  if
    drop false
  else
    dup ( decimal ) 10 <
    if
      (base)
    else
      dup ( decimal ) 9 <
      if
        drop false
      else
        ( decimal ) 7 -
        (base)
      then
    then
  then ;

\ >NUMBER : u addr len -- u addr len
: >number
  begin
    dup 0> if over c@ >digit else false then
  while
    unrot >r >r >r
    base @ u* r> +
    r> char+ r> 1-
  repeat ;

\ ?NUMBER : caddr -- n true | false
: ?number
  0 swap
  count
  \ 0 addr len
  \ obtain sign
  over c@ [char] - =
  \ 0 addr len -- flag
  if  1- swap char+ swap  -1 else 1 then >r
  \ 0 addr len
  >number
  \ u addr len --
  nip
  \ u len --
  \ apply sign
  swap r> +- swap
  \ remaining length should be 0
  dup if nip then
  0=
  ;
