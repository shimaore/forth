\ U/MOD : u1 u2 -- rem quot
: u/mod
  1 >r >r 0
  \ R: quot u2 --
  \ S: rem-l(u1) rem --
  begin
    swap dup +c
    rot 2* or
    r@ - dup 0<
    if r@ + 0 else 1 then
    r> swap r>
    dup +c >r or
    r> swap >r swap >r
  until
  nip rdrop r>
  ;
