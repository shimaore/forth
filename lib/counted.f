
\ example from ansi forth spec
\ compiles and works as-is. :)

: docolon ( -- )       \ modify created word to execute like a colon def
     does> ( i*x a-addr -- j*x )
     1 over +!         \ count executions
     cell+ @ execute   \ execute :noname definition
;

: old: : ;             \ just an alias

old: : ( "name" -- a-addr xt colon-sys )
                       \ begins an execution-counting colon definition
     create  here 0 ,  \ storage for execution counter
     0 ,               \ storage for execution token
     docolon           \ set run time for created word
     :noname           \ begin unnamed colon definition
;

old: ; ( a-addr xt colon-sys -- )
                      \ ends an execution-counting colon definition
    postpone ;        \ complete compilation of colon def
    swap cell+ !      \ save execution token
;  immediate


