

\ So the idea is as follows:
\ MACRO x
\  defines a name 'x' which when used in a compile state
\    will create a list of locations where it appears in that code
\
\ MACRO-NAME name1
\ MACRO-NAME name2
\ :MACRO name
\   ... name1 ... name2 ...
\ ;MACRO
\
\ xt1 MACRO name -> xt2  ( name with xt1 substituted )
\

variable last-macro

0 last-macro !

: macro-name
    create immediate
      here 
        0 ,      \ list of locations where substitution need to arise
      last-macro
        dup @ ,  \ list of macros defined since the last ;MACRO
      !
    does>
      \ appends this entry to the list of entries that will need to be resolved.
      dup @ ,
      here swap !
;
      

: ;macro
  postpone ;
  
  
  


: 



 : name  >r .....  r@ execute .... rdrop ;

