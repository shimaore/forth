\ MOVE : addr-src addr-dest u --

: move
  begin
    ?dup
  while
    1- >r
    2dup swap @ swap !
    cell+ swap cell+ swap
    r>
  repeat
  2drop ;
