
\ Create a word which, when executed, removes all further
\ definitions, including itself.

: marker
  latest here
  create
    ,
    ,
  does>
    dup @ dp !
    cell+ @ _latest !
  ;

