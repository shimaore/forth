.( Compiling defer.f: )

\ _DEFER is a linked-list of (addr,value) pairs where the value must
\ be stored at addr at a later time.

variable _defer
0 _defer !

\ DEFER! adds an entry in the linked-list

: defer!  ( value addr -- )
  _defer @
  here _defer !
  ( link ) , ( addr ) , ( value ) ,
  ;

\ RESOLVE-DEFERRED resolves the pairs previously stored in the list by DEFER!
\ and stores the values at their respective target addresses.

: resolve-deferred
  _defer @
  0 _defer !
  begin
    ?dup
  while
    dup @ swap ( next current -- )
    cell+ dup cell+ @ ( value ) swap @ ( addr )
    \ next line is a no-op, for debugging purposes only
    2dup ." resolvind deferred: " swap u. u. ." !" cr
    \ this is the actual operation
    !
  repeat
  ;

.( done ) cr
