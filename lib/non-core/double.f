.( Compiling double.f: )

: 2over
  r> r> 2dup
  r> unrot
  r> unrot ;

: 2swap
  >r unrot
  r> unrot ;

: 2nip
  >r >r 2drop r> r> ;

: 2!  tuck ! cell+ ! ;
: 2@  dup cell+ @ swap @ ;

: 2r@
  r>
     r> r@
     over >r
     swap
  rot >r ;

: 2r>
  r>
    r> r> swap
  rot >r ;

: 2>r
  r> unrot
    swap >r >r
  >r ;


: dliteral swap [literal] [literal] ; immediate
: 1.  1 0 ;
: 0.  0 0 ;
: d+c rot +c >r unrot +c rot +c r> + ;
: d+  rot +     unrot +c rot +       ;
: d1+ 1. d+ ;
: d2* 2dup d+ ;
: 0.= or 0= ;
: 0.< nip 0< ;
: dnegate invert swap invert swap d1+ ;
: d+- 0< if dnegate then ;
: dabs dup d+- ;
: d-  dnegate d+ ;
: s>d  dup 0< if -1 else 0 then ;
: 2or  swap >r or swap r> or swap ;

\ um* : u1 u2 -- ud
: um*  
   2dup > if swap then
   0 2>r
   0. rot
   begin ?dup while
     dup 1 and
     if unrot 2r@ d+ rot then
     2r> d2* 2>r
     2/
  repeat
  rdrop rdrop
  ;

\ udm+ : ud1 u -- ud2
: udm+  rot +c rot + ;

\ udm* : ud1 u -- ud2
: udm* rot over um* >r >r um* drop 0 swap r> r> d+ ;

: m*
  2dup xor >r
  abs swap abs um*
  r> d+- ;

\ dm/mod : ud1 ud2 -- drem dquot
: dm/mod
  ." dm/mod not implemented." cr abort
  1. 2>r 2>r 0.
  \ r: dquot ud2 --
  \ s: rem-l(ud1) drem --
  begin
    2swap 2dup ( d+c ) swap dup +c >r swap +c swap r> +c rot +
    2rot d2* 2or
    2r@ d- 2dup 0.<
    if 2r@ d+ 0. else 1. then
    2r> 2swap 2r>
    2dup d+c 2>r 2or
    2r> 2swap 2>r 2swap 2>r
  until
  2nip 2rdrop r>
  ;

\ um/mod : ud u -- rem quot
: um/mod
  0 dm/mod drop swap drop ;

\ m/mod : ud u -- rem dquot
: m/mod
  0 dm/mod rot drop ;


: # base @ m/mod rot digit> hold ;
: #> 2drop hld @ pad over - ;
: #s begin # 2dup 0.= until ;
: d.  dup >r dabs <# #s r> sign #> type space ;
: d.r >r dup >r dabs <# #s r> sign #> r> over - spaces type ;
: u.r 0 swap d.r ;
: .r  >r s>d r> d.r ;

\ >dnumber : d addr1 ul -- d addr2 ul
: >dnumber
  begin
    dup if over c@ >digit else false then
  while
    unrot >r >r >r
    base @ udm*
    r> 0 d+
    r> char+
    r> 1-
  repeat ;

variable dpl

\ ?dnumber : caddr -- d true | false
: ?dnumber
  -1 dpl !
  0. rot
  count
  over c@ [char] - =
  if 1- swap char+ swap -1 else 1 then >r
  >dnumber
  over c@ [char] . =
  if 1- swap char+ swap
    dup >r
    >dnumber
    dup r> - dpl !
  then
  nip
  unrot
  r> d+-
  rot
  \ remaining length should be 0
  dup if nip nip then
  0= 
  ;

:noname ( number, )
   ?dnumber
   if
      dpl @ 1+
      state @
      if
         if        [compile] dliteral
         else drop [compile] literal
         then
      else
         if else drop then
      then
   else
      here count type ." ? "
   then ;
number, !

.( done ) cr
