.( Compiling debugger.f: )

\ symbol: returns the word (and offset in word execution definition)
\         from an address.

: symbol ( n -- )
  >r
  latest
  begin
    dup r@ >
  while
    (nfa>lfa) @
  repeat
  r@ over (nfa>pfa) - ?dup if r> . ." = " . ." + " else rdrop then (nfa-count) type 
  ;

decimal
: disassemble ( n -- )
  begin
    dup .
    dup @ 1 u>  \ 0 = bye, 1 = exit
  while
    dup @
        dup 32 < 
        if   ." ( " . ." )"
        else symbol
        then
    cr
    cell+
  repeat
  @ if ." exit" else ." bye" then cr
  ;

.( done ) cr
