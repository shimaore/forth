\ see eg http://www.taygeta.com/forth/dpansa15.htm#a.15.6.2.2533

: [else]  ( -- )
    1 begin                               \ level
      begin
        bl word count  dup  while         \ level adr len
\        2dup  s" [if]"  compare 0=
        2dup  s" [if]"  s=
        if                                \ level adr len
          2drop 1+                        \ level'
        else                              \ level adr len
          2dup  s" [else]"
\          compare 0= if                   \ level adr len
          s= if                           \ level adr len
             2drop 1- dup if 1+ then      \ level'
          else                            \ level adr len
\            s" [then]"  compare 0= if     \ level
            s" [then]"  s= if             \ level
              1-                          \ level'
            then
          then
        then ?dup 0=  if exit then        \ level'
      repeat  2drop                       \ level
    refill 0= until                       \ level
    drop
;  immediate

: [if]  ( flag -- )
0= if postpone [else] then ;  immediate

: [then]  ( -- )  ;  immediate

