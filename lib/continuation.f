\
\ Example:
\ 1000 100 10 1 ' + 3 repeat .
\

: repeat ( xt count -- )
  begin  \ S: xt count --
    dup
  while
    >r
      dup >r
      execute
      r>
    r> 1-
  repeat
  2drop ;

\
\ Tools for continuations
\

: stack, ( args u -- )
  dup ,  \ save the depth
  \ ['] , SWAP repeat
  begin  \ S: args u --
    dup
  while
    >r
      ,
    r> 1-
  repeat
  drop
  ;

: unstack ( addr -- args )
  dup @
  begin  \ S: addr-1 u --
    dup
  while
    >r 
      cell+ dup >r
      @
      r>
    r> 1-
  repeat
  2drop
  ;

\
\ Example:
\   function 2+
\   2 1 ' + continuation to 2+
\

: continue
  r> dup @ >r \ replace the return address with the xt
  cell+ unstack
  ; \ EXECUTE

: continuation ( args u xt -- xt )
  here >r
  postpone continue
  compile,  \ saves the xt
  stack,    \ saves the args
  r>        \ return the new xt
  ;


