\ This is a "macro-assembler" source for myforth.
\ (c) 2005-2020, Stephane Alnet
\ ------------

__abort:
  start

: _abort  ( image entry point, see run/interpreter.f )
    lit __abort       exit

: type ( addr len )
  ?dup
  0branch exit_drop
  over c@ emit
  1- swap char+ swap
  branch type

: dp      ( current dictionary pointer )
    lit __dp      exit

: char-dp      ( current dictionary pointer )
    lit __char-dp      exit

( The `char` and `cell` memories are separate. )
__dp:
    last_cell
__char-dp:
    last_char

: state   ( current interpreter state: compiling if non-zero )
    lit __state   exit
__state:
    0

: _latest ( DP value of last defined word )
    lit __latest  exit
__latest:
    last_word

: pfa     ( PFA of word currently been defined )
    lit __pfa     exit
__pfa:
    0

: nfa     ( NFA of word currently been defined )
    lit __nfa     exit
__nfa:
    0

: tib     ( The Input Buffer )
    lit __tib     exit
__tib:
    0

: >in     ( Cursor offset in TIB )
    lit __>in     exit
__>in:
    0


\ ------------

\ The TIB uses 1024 characters ..
.inline tib_size    1024
\ .. starting 1024 characters after the current location of HERE.
.inline here_to_tib 1024
\ See below for the definition of PAD (HERE+256 normally).
\ HOLD uses the PAD downwards (towards HERE), for a maximum of 255
\ characters.
\ WORD uses the PAD upward, for a maximum of 767 characters.
\ (Note: the last character of the PAD may actually get overwritten
\ by WORD .)

((
  These are some very-easy-to-define words.

  : 2*     DUP + ;
  : 2DUP   OVER OVER ;
  : 2DROP  DROP DROP ;

  : ROT    >R SWAP R> SWAP ;
  : UNROT  SWAP >R SWAP R> ;
  : TUCK   SWAP OVER ;
  : NIP    SWAP DROP ;
  : +!     TUCK @ + SWAP ! ;
))

\ : 2*
\     dup + exit
: 3*
    dup 2* + exit
: 4*
    2* 2* exit
: 5*
    dup 4* + exit
: 10*
    2* 5* exit
\ : 2dup  \ a b -- a b a b
\     over over exit
\ : 2drop \ a b --
\     drop
exit_drop:
    drop exit

: rot   \ a b c -- b c a
    >r swap r> swap exit
: unrot \ a b c -- c a b
    swap >r swap r> exit
: tuck  \ a b -- b a b
    swap over exit
\ Implemented as opcode
\ : nip   \ a b -- b
\     swap drop exit
\ : +!
\     tuck @ + swap ! exit

: char+
    1+ exit
: char-
    1- exit
: cell+
    lit 1 cells + exit
: cells
    \ cell memory is separate from char memory
    exit

((
  DP is the address of the memory location containing the current
  dictionary pointer. HERE returns the value of the dictionary pointer.
  XXX: Should be redefined to use VOCABULARY, etc.

    : HERE DP @ ;
))

: here
    dp @ exit

: char-here
    char-dp @ exit

((
  : ALLOT      CELLS DP +! ;
  : CALLOT     CHAR-DP +! ;
  : ,          HERE !  1 ALLOT ;
  : COMPILE,   , ;
  : [LITERAL]  POSTPONE LIT , ;
  : LITERAL    [LITERAL] ; IMMEDIATE
))


: allot   \ n --
    cells dp +! exit

: ,       \ n --
    here
    lit 1 allot
    !
    exit

: callot
    char-dp +! exit

: compile, \ xt --
    branch ,

: [literal] \ n --
    lit lit compile, \ POSTPONE LIT
    branch ,


((
  : -      NEGATE + ;
  : 1-     -1 + ;
  : 0<>    0= INVERT ;
  : 0<     neg_mask AND 0<> ;
  : <      - 0< ;
  : =      - 0= ;
  : <>     = INVERT ;
  : >      SWAP < ;
))

\ Implemented as opcode
\ : invert
\     true xor exit
\ : negate
\     invert 1+ exit

\ : -
\     negate + exit
\ : 1-
\     lit -1 + exit

: 0<>
    0= invert exit
\ : 0<
\     neg_mask and
\     branch 0<>
: <
    - 0< exit
: =
    - 0= exit
: <>
    = invert exit
: >
    swap < exit


((
  LATEST returns the NFA of the last defined word.
  [ puts the interpreter in evaluation mode.

  : LATEST _LATEST @ ;
  : [      0 STATE ! ; IMMEDIATE
))


: latest  \ -- laddr
    _latest @ exit
:imm [
    lit 0 state ! exit


((
  QUIT is the main read-eval loop.

 : QUIT
   [COMPILE] [
   BEGIN
     REFILL
   WHILE
     0 >IN !
     INTERPRET
   REPEAT ;
))


quit:
    \ Start in execute mode
    [
quit.begin: \ BEGIN
    refill
    0branch exit_now \ WHILE
    lit 0 >in !
    interpret
    branch quit.begin \ REPEAT


((
  INTERPRET is the main interpreter.

  This version only works with unsigned decimal numbers
  -- no BASE support, no sign support, no DOUBLE support --
  but is otherwise fully functional. It is used to
  compile the other bootstrap files.

  : INTERPRET
    BEGIN
      BL WORD
      ?DUP
    WHILE
      FIND
      ?DUP
      IF
        STATE @
        IF  SWAP @ EXECUTE ELSE NIP EXECUTE THEN
      ELSE
        DUP ?UNUMBER
        IF
          NIP
          STATE @
          IF [COMPILE] LITERAL THEN
        THEN
      THEN
    REPEAT ;
))

interpret:
    \ BEGIN
    bl word
    ?dup
    0branch exit_now \ WHILE
    find ( caddr -- cpfa pfa | caddr 0 )
    ?dup
    0branch interpret.not_found \ IF
    state @
    0branch interpret.execute \ IF
    swap @ \ execute the xt of the cpfa, and provide it with the pfa
    execute ( pfa xt -- )
    branch interpret \ THEN
interpret.execute:
    nip ( cpfa pfa -- pfa )
    execute
    branch interpret \ THEN
interpret.not_found: \ ELSE   ( caddr )
    dup ?unumber
    0branch unknown-word \ IF
    nip state @
    0branch interpret \ IF
    [literal]
    branch interpret \ THEN

unknown-word:
    count type
    lit '?' emit bl emit
    bye

((
  REFILL : -- more-to-come-flag

  : REFILL
    CHAR-HERE here_to_tib + TIB !
    TIB @ tib_size ACCEPT
    TIB @ + 0 SWAP C!
    EOF INVERT ;
))

: refill
    char-here lit here_to_tib + tib !
    tib @ lit tib_size accept
    \ +n2 --
    tib @ + lit 0 swap c!  \ appends a 0 as end of string marker
    eof invert
    exit


((
: ACCEPT  \ addr +max -- +n2
  0 BEGIN
    2DUP >
    DUP IF DROP GET-KEY THEN
    DUP NEWLINE <> AND
    ?DUP
  WHILE
    >R
    ROT
      R> OVER C! CHAR+
    UNROT
    1+
  REPEAT
  NIP NIP
  ;
))

: accept \ addr +max -- +n2
    lit 0
accept.begin: \ BEGIN
    \ addr max pos --
    2dup >
    dup
    0branch accept.2 \ IF
    drop
    get-key  \ GET-KEY returns 0 at EOF
accept.2: \ THEN
    \ -- addr max pos 0|key
    dup newline <> and
    dup carriage <> and
    ?dup
    0branch accept.exit \ WHILE
    dup backspace <> and
    ?dup
    0branch accept.backspace
    \ addr n1 n2 c --
    >r
    rot
    \ n1 n2 addr --
    r> over c! char+
    \ n1 n2 addr+1 --
    unrot
    \ addr+1 n1 n2 --
    1+
    branch accept.begin \ REPEAT
accept.backspace:
    \ -- addr max pos
    dup
    0branch accept.begin
    1- rot char- unrot
    branch accept.begin
accept.exit:
    nip nip
    exit


: newline
    lit 10 exit
: carriage
    lit 13 exit
: backspace
    lit 8 exit


((
  \ No sign support, only base 10
  : ?UNUMBER
    0 SWAP
    COUNT
    >NUMBER
    NIP
    DUP IF NIP THEN
    0=
    ;
))

?unumber: \ addr -- u true | false
    lit 0 swap
    count
    >number
    \ u addr len
    nip
    \ u len
    dup
    0branch exit_0= \ IF
    nip
exit_0=: \ THEN
    0=
    exit


((
  \ Only supports base 10
  : >NUMBER
    BEGIN
      DUP
      IF OVER C@ >DIGIT ELSE FALSE THEN
    WHILE
      UNROT >R >R >R
      10* R> +
      R> CHAR+ R> 1-
    REPEAT ;
))

>number:
    \ BEGIN : u addr len -- u addr len
    dup
    0branch exit_now \ IF
    over c@ >digit
    0branch exit_now \ WHILE
    \ -- u addr len digit
    unrot
    \ -- u digit addr len
    >r >r >r         \ -- u  r: -- len addr digit
    10*
    r> +
    r> char+
    r> 1-
    branch >number \ REPEAT


((
  \ Only supports base 10
  : >DIGIT
    [CHAR] 0 -
    DUP 0< INVERT
    OVER 10 < AND
    DUP INVERT IF NIP THEN ;
))

>digit: \ c -- u true | false
    lit '0' -
    dup 0< invert
    over lit 10 <
    and \ u flag --
    dup invert
    0branch >digit.exit
    nip
>digit.exit:
    exit


((
  EXECUTE executes the code at the provided address.
  (The address should be a PFA.)

  : EXECUTE  >R ;
))

: execute  ( xt -- )
    >r
    exit


((
  Reads a delimited word from the input stream at the current position
  (as indicated by TIB + >IN) and returns a pointer to a (length,start)
  cells pair.

  : WORD
    TIB @ >IN @ + SWAP ENCLOSE
    >IN +!
    OVER - >R
    + 1-
    R@ IF R> OVER C! ELSE DROP R> THEN ;
))

\ The PAD starts at 256 characters after CHAR-HERE and
\ goes down (hence can store at most 255 characters).
\ (Remember that the TIB starts
\ 1024 characters after CHAR-HERE and goes up.)

: pad
    char-here
    lit 256 +
    exit

: word  \ c "cXXXc" -- caddr | 0
    tib @ >in @ + swap enclose \ -- addr n1 n2 n3
    >in +!
    over - \ -- addr n1 length
    >r     \ -- addr n1
    +
    r@     \ -- addr length
    0branch word.exit \ IF
    \ Since we have separate Cell and Char memories, we cannot use
    \ the address of the pad to store the temporary 2 Cells.
    \ (These might be copied later into the NFA by WORD, .)
    \ So we arbitrarily take an address some space ahead of HERE.
    here lit 64 + \ -- addr caddr
    r> over !     \ store the length first  -- addr caddr
    swap          \ -- caddr addr
    over cell+ !  \ store the addr(c) second
    \ -- caddr
    exit
word.exit:
    drop  \ --
    r>    \ -- 0
    exit


((
: ENCLOSE=
  >R
  BEGIN
    DUP C@
    DUP IF R@ = THEN
  WHILE CHAR+ REPEAT
  RDROP
  ;

: ENCLOSE<>
  >R
  BEGIN
    DUP C@
    DUP IF R@ <> THEN
  WHILE CHAR+ REPEAT
  RDROP ;

: ENCLOSE
  >R DUP
  DUP
  R@ ENCLOSE=
  2DUP SWAP - UNROT
  R@ ENCLOSE<>
  2DUP SWAP - UNROT
  R> ENCLOSE=
  SWAP -
  ;
))


: enclose  \ addr c -- addr n1 n2 n3
    >r dup
    dup
    r@ enclose=
    2dup swap - unrot
    r@ enclose<>
    2dup swap - unrot
    r> enclose=
    swap -
    exit

: enclose= \ addr1 c "cccX" -- addr2
    >r
enclose=.1: \ BEGIN
    dup c@
    dup
    0branch enclose=.2 \ IF
    r@ =
enclose=.2: \ THEN
    0branch exit_rdrop \ WHILE
    char+
    branch enclose=.1 \ REPEAT
exit_rdrop:
    rdrop
exit_now:
    exit


: enclose<> \ addr1 c "XXXc" -- addr2
    >r
enclose<>.1: \ BEGIN
    dup c@
    dup
    0branch enclose<>.2 \ IF
    r@ <>
enclose<>.2: \ THEN
    0branch exit_rdrop \ WHILE
    char+
    branch enclose<>.1 \ REPEAT


((
: FIND
  LATEST (FIND-NFA)
  DUP
  IF NIP DUP NFA>PFA SWAP ?IMMEDIATE THEN ;
))

: find  ( caddr -- cpfa pfa | caddr 0 )
    latest (find-nfa)       ( -- caddr nfa | caddr 0 )
    dup
    0branch exit_now \ IF
    nip  ( -- nfa )
    dup nfa>cpfa
    swap nfa>pfa
    exit


((
: NFA>LFA   3 + ;
: NFA>PFA   NFA>LFA CELL+ ;
))

: nfa>lfa ( nfa --- laddr )
   cell+ cell+ exit

: nfa>cpfa ( nfa --- cpfaddr )
  nfa>lfa cell+ exit

: nfa>pfa ( nfa --- xt )
  nfa>cpfa cell+ exit


((
: (FIND-NFA)  ( caddr laddr -- caddr nfa | caddr 0 )
  BEGIN
    DUP
    IF 2DUP C= INVERT
    ELSE FALSE
    THEN
  WHILE
    NFA>LFA LFA@
  REPEAT
  ;
))

: (find-nfa) ( caddr laddr -- caddr nfa | caddr 0 )
    \ BEGIN
    dup
    0branch exit_now \ IF
    2dup c= invert
    0branch exit_now \ WHILE
    nfa>lfa @
    branch (find-nfa) \ REPEAT


((
: S=
  ROT
  OVER <>  \ must have the same length
  IF
    NIP 2DROP FALSE
  ELSE
    (S=)
  THEN ;

: (S=)  \ addr1(c) addr2(c) len -- bool
  BEGIN
    DUP >R
    IF 2DUP C@ SWAP C@ = ELSE FALSE THEN
  WHILE
    CHAR+ SWAP CHAR+
    R> 1-
  REPEAT
  2DROP R> 0=
;

))

: (s=)  \ addr1 addr2 len -- bool
    dup >r
    0branch (s=).exit \ end of string
    2dup c@ swap c@ =
    0branch (s=).exit \ WHILE
    char+ swap char+
    r> 1-
    branch (s=) \ REPEAT
(s=).exit:      \ -- addr1 addr2   r: -- len
    2drop r> 0=
    exit

: s=    \ addr1 len1 addr2 len2 -- bool
    rot          \ addr1 addr2 len2 len1
    over <>      \ addr1 addr2 len2 bool
    0branch (s=) \ IF
    drop 2drop lit 0
    exit


\ : C= >R COUNT R> COUNT S= ;
: c=    \ caddr1 caddr2 -- bool
    >r count r> count
    branch s=


((
  : BL 32 ;
  : 2@ DUP CELL+ @ SWAP @ ;
  : COUNT 2@ ;
  : ?DUP DUP IF DUP THEN ;
))

: bl
    lit 32 exit

: 2@
    dup cell+ @ swap @ exit

: count \ caddr -- addr(c) len
    branch 2@

: ?dup
    dup
    0branch ?dup.exit \ IF
    dup
?dup.exit:  \ THEN
    exit


start: \ :NONAME
    sp!
    rp!
    quit
    \ rdrop \ required for all interrupts
    bye

((
  With all the words defined up to this point, we actually have a working
  interpreter that will start, accept input, recognize numbers and some
  words. The major drawback is that since the opcodes have not been
  defined as words in the dictionary, we will not be able to use them --
  which means we won't be able to do much.
  (As a sidenote, I would like to point out that there are actually all
  the required words to keep adding CHARs and CELLs to the dictionary, so
  in theory one could keep building the dictionary manually until the point
  where the following words are defined. I haven't tried.)

  To summarize:
  The following words deal with defining new words to simplify further
  extension of the dictionary, and specifically provide the ability to
  compile opcodes.f.
))

((
 : (FIND) BL WORD DUP IF FIND THEN ;
))

: (find)  ( "word" -- cpfa pfa | caddr 0 )
    bl word dup
    0branch exit_now \ IF
    branch find


((
  : POSTPONE
    (FIND) ( -- cpfa pfa --- assumed )
    postpone literal
    @ postpone literal
    postpone execute
    ; immediate
))

:imm postpone
    (find)            ( -- cpfa pfa | caddr 0 )
    ?dup
    0branch unknown-word
    [literal]         \ [LITERAL] (pfa) -- will provide the PFA to the CPFA
    @ [literal]       \ @ [LITERAL] (cpfa)  -- by default `compile,` but could be anything
    lit execute compile, \ POSTPONE EXECUTE
    exit


((

  CMOVE is required here in order to move the word from the TIB to the
  dictionary (HERE).

  : CMOVE
    BEGIN
      ?DUP
    WHILE
      1- >R
      2DUP SWAP C@ SWAP !
      CHAR+ SWAP CHAR+ SWAP
      R>
    REPEAT
    2DROP ;


))

((

: cmove ( src dst count -- )
    \ BEGIN
    ?dup
    0branch 2drop \ WHILE
    1- >r
    2dup swap c@ swap c!
    char+ swap char+ swap
    r>
    branch cmove \ REPEAT

))
((

   Dictionary structure:

   ' WORD, ' will append a name to the dictionary.
   The word is stored in the CHAR memory.
   In the cell memory we store the length and the char-addr of the
   string.

   Instead of using FLAGS we store two PFAs, one for the code in
   interpret mode (our current PFA), one for the code in compile mode (CPFA).
   IMMEDIATE copy the address of the interpret mode PFA into the CPFA,
   while a new word (`cpfa!`) was added to allow to set the the CPFA distinct from
   the interpretive PFA. (By default, new words will use a CPFA that compile a
   call to the PFA, as expected.)

   The layout is as follows:

    (end of previous word)
    NFA: 2 cells
        length
        address of start of string
    LFA: 1 cell
        link to previous NFA
    CPFA: 1 cell
        xt of compile-time function (that function will receive the PFA as its parameter)
    (start of executable code)
    PFA:
        start of executable code

   For a `:noname` definition, the NFA, LFA, and CPFA do not exist.

   Following the name is the pointer that links to the previously
   defined name. (Dictionary entries are thus in a linked list.)
   This field is called the LFA.
   Note:  :NONAME words do not have a name nor an LFA since they
          do not belong to the dictionary.

   : LFA,     ALIGN LATEST , ;

   There is no CFA since all words are executable,
   and in the virtual machine, opcodes are differentiated by value.
   (So there is no need for a CFA to differentiate sub-calls vs opcodes.)

   Therefore, immediately following the LFA are the CPFA and the PFA.

   : PFA,     ALIGN HERE PFA !  ;

   The first CELL of the PFA is always an instruction (either an opcode or
   a sub-call).
))

: word, ( caddr -- nfa )
    char-here >r    \ start of string in dictionary (dst)
    count           \            caddr -- addr len
    dup callot      \ allocate the memory for the string
    swap over       \                  -- len addr len
    r@ swap         \                  -- len addr dst len
    cmove           \ copy the string  -- len
    \ align         \ NFA
    here swap       \                  -- nfa len
    ,               \ compile length   -- nfa
    r>              \                  -- nfa dst
    ,               \ compile addr(c)
    exit

: lfa,
    latest , exit
: cpfa,
    lit compile, compile, \ postpone compile,
    exit
: pfa!
    here pfa ! exit


((

  LATEST! is called at the end of a definition in order to update the
  head of the linked-list that contains the names of the words.
  (That linked-list is what we call the dictionary.)
  Note: Words defined by :NONAME do not belong to the dictionary (they
  are anonymous). In the case of a word defined by :NONAME, NFA will
  contain 0, and LATEST! will not update the head of the list.

  : LATEST!  NFA @ ?DUP IF _LATEST ! THEN ;

))

: latest!
    nfa @ ?dup
    0branch latest!.exit
    _latest !
latest!.exit:
    exit

((

  HEADER creates a complete named definition starter for a word,
  including name, LFA, and PFA.

  : HEADER
    HERE NFA !
    BL WORD
    \ should error if 0
    WORD,
    LFA,
    PFA,
    ;

))

: header
    bl word
    ?dup
    0branch unknown-word
    word, nfa !
    lfa, cpfa, pfa! exit

((

  Switches from evaluation mode to compilation mode.

  : ] true STATE ! ;

))

: ]
    true state ! exit

((

  : starts the definition of a new word that is meant to be executed later.

  : : HEADER ] ;

))

: :
    header ] exit

((

   ; terminates a definition started by : or :NONAME, and switches back
   to evaluation mode.

   : ;
     POSTPONE EXIT
     LATEST!
     [COMPILE] [
     ALIGN
     0 PFA !       \ clear the PFA pointer
     ; IMMEDIATE

))

:imm ;
    lit exit compile,   \ POSTPONE EXIT
    latest!
    [
    lit 0 pfa !
    exit

((

  IMMEDIATE marks the last word as immediate.

  In effect, it compiles `execute` in the cpfa.

  : IMMEDIATE ' EXECUTE LATEST NFA>CPFA ! ;

))

: immediate
    lit execute immediate! exit

: immediate! ( xt --- )
    latest nfa>cpfa ! exit


((

  The CREATE .. DOES> couple.

  The executable part of the PFA of CREATE only uses one CELL, which is
  immediately followed by the data area of the word.

  CREATE
  If the word is simply created by CREATE but never completed by DOES>,
  then the PFA cell contains a call to (CREATE), which will pop its return
  address (=the address of the CELL following the PFA) onto the data stack,
  and return the call to the calling word, in effect putting the address
  of the data area on the data stack.

  A word created by CREATE looks like:

    PFA:
         (CREATE)    \ compiled by CREATE
         <data>      \ location of >BODY

  CREATE .. DOES>
  If the word is created by CREATE and then completed by DOES>, DOES>
  will compile (DOES>) and then R> in the creating word. (DOES>) is the
  last word executed after CREATE, and it will update the PFA of the word
  that has just been created with the address that follows (DOES>) in the
  creating word. In other words, the PFA of the word created by CREATE
  will now contain a call to the word that follows (DOES>), which is R>
  (and then the words following R> in the original definition of the creating
  word). At the time the virtual machine encounters R>, the return stack
  will contain the address following the PFA, and R> will put that address
  onto the data stack. Then the execution continues in the creating word.

  In other words, the creating word is rewritten as:

        CREATE
        ...
        (DOES>)   \ DOES>  part 1
    next:
        R>        \ DOES>  part 2
        ...
        ;

  A word created by CREATE .. DOES> looks like:

    PFA:
        <next:>   \ as above, compiled by (DOES>)
        <data>    \ location of >BODY


  >BODY gives the data area of a word created by CREATE based on its PFA.
  Since the PFA is only one cell long in all cases, the data area starts
  at the first CELL after the PFA.

  \ (CREATE) is the default PFA for a word created by CREATE
  : (CREATE) R> ;

  : CREATE
    HEADER
    POSTPONE (CREATE)  \ default PFA
    LATEST!
    ;

  \ (DOES>) replaces the default PFA
  : (DOES>)
    R> PFA @ !         \ replace the default PFA
    0 PFA !            \ clear the PFA pointer
    ;

  : DOES>
    POSTPONE (DOES>)   \ last instruction of the defining word
    POSTPONE R>        \ first instruction of the defined word
    ; IMMEDIATE

  \ >BODY
  \ returns a pointer to the data section of a word created by CREATE
  : >BODY  CELL+ ;


  Note: DOES> and >BODY themselves are not defined here since we don't
        need them right away. See lib/does.f.

))

: (create)
    r> exit

: (does>)
    r> pfa @ !
    lit 0 pfa !
    exit

: create
    header
    lit (create) compile,  \ POSTPONE (CREATE)
    latest!
    exit


((

  :opcode is used to register the opcodes.

  A straightforward way to define the opcodes would be to simply create
  words that contain (1) the actual opcode and (2) the opcode for EXIT.
  However this doesn't work well because this results in a lot of subcalls,
  when the opcodes were created to be used natively (their space overlaps
  with the memory space). Also EXIT cannot be defined that way.

  Instead, :opcode defines opcode words that will contain:
  (a) an executable version of the opcode, to be used interactively;
      that version uses the "opcode + EXIT" schema since timing is less a
      constraint when used interactively;
  (b) a compilation version that will COMPILE, the opcode in the current
      definition.

  :opcode stores the opcode value as the first CELL of the data section
  ('BODY') provided by CREATE, and follows it by an explicit EXIT.
  In the DOES> section, the BODY is used to either
  (a) EXECUTE the opcode and the EXIT, or
  (b) retrieve the value of the opcode and add it to the definition of
      the word being compiled.

  : :opcode CREATE COMPILE, POSTPONE EXIT IMMEDIATE
            DOES> STATE @ IF @ COMPILE, ELSE EXECUTE THEN ;

))

\ FIXME rewrite: an opcode simply has a CPFA that compiles the value of the opcode, while its (interpretive-time) PFA/xt contains opcode + exit

((

  : (:opcode) ( pfa --- )
    @ compile,
    exit

))

\ Note: last_word is used to initialize LATEST (see top of this file).

\ In this version, last_word is computed by the assembler.

: :opcode  ( n 'name' --- )
    create
    compile,
    lit exit compile,   \ POSTPONE EXIT
    immediate
    (does>)   \ DOES>
opcode.does:
    r>        \ DOES>
    state @
    0branch opcode.execute
    @ compile,
    exit
opcode.execute:
    execute
    exit

((
  The words created by :opcode will therefor look like this:

  for example for    n_DUP :opcode DUP

  :imm DUP
    opcode.does \ installed by DOES>
    n_DUP
    EXIT
    (DOES>)

))

((

  At this point we have a complete base system that can compile opcodes.

))
