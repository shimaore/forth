#ifndef BASE_MEMORY_H
#define BASE_MEMORY_H

#include <stdlib.h> /* malloc() */
#include <string.h> /* memmove */
#include "params.h"

#define forth_malloc(sz) malloc(sz)
#define forth_free(p)    free(p)

#define forth_new(type,sz)  ((type*)forth_malloc(sz*sizeof(type)))

typedef struct f_memory {
  g_cell* cells;
  g_char* chars;
  g_float* floats;
} f_memory;

void initiate_mem( f_memory* p );

inline g_cell* cell_page_zero( f_memory* p )
{
  return p->cells;
}

inline g_char* char_page_zero( f_memory* p )
{
  return p->chars;
}

inline void __move( f_memory const* p, g_cell dst, g_cell src, g_cell len )
{
  memmove( p->cells+dst, p->cells+src, len*sizeof(g_cell) );
}

inline void __cmove( f_memory const* p, g_cell dst, g_cell src, g_cell len )
{
  memmove( p->chars+dst, p->chars+src, len*sizeof(g_char) );
}

inline g_cell __peek( f_memory const* p, g_cell a )
{
  return p->cells[a];
}

inline g_char __cpeek( f_memory const* p, g_cell a )
{
  return p->chars[a];
}

/* Note: with the new architecture, the poke|cpoke are not guaranteed
 * to be successful. However they are guaranteed to finish.
 */

inline void __poke( f_memory const* p, g_cell a, g_cell v )
{
  p->cells[a] = v;
}

inline void __cpoke( f_memory const* p, g_cell a, g_char v )
{
  p->chars[a] = v;
}

#endif
