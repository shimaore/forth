/*
 * (c) 2003-2020 Stephane Alnet
 */
#ifndef __BASE_H
#define __BASE_H

#include "params.h"
#include "memory.h"

typedef g_char console_getkey();
typedef void   console_emit(g_char);
typedef g_cell console_eof();

typedef struct console_io {
  console_getkey* getkey;
  console_emit  * emit;
  console_eof   * eof;
} console_io;

/* Memory initialization (used by the bootstrap loader). */

/* Main loop, single thread. */
int run( f_memory*, console_io* );

#endif /* __BASE_H */
