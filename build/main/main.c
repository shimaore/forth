#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "base.h"
#include "loader.h"
#include "memory.h"

#include "esp_system.h"

extern unsigned char memory[]      asm("_binary_memory_start");
extern unsigned char memory_end[]  asm("_binary_memory_end");

extern unsigned char source[]      asm("_binary_source_start");
extern unsigned char source_end[]  asm("_binary_source_end");

#include "esp_log.h"
#include "esp_system.h"
#include "esp_vfs_dev.h"

// See https://docs.espressif.com/projects/esp-idf/en/stable/api-reference/peripherals/uart.html
#include "driver/uart.h"

    // Probably migrate to main? FIXME
    // See https://github.com/espressif/esp-idf/blob/master/examples/system/console/main/console_example_main.c#L60

void configure_uart()
{
    /* Configure UART. Note that REF_TICK is used so that the baud rate remains
    * correct while APB frequency is changing in light sleep mode.
    */
    const uart_config_t uart_config = {
            .baud_rate = CONFIG_ESP_CONSOLE_UART_BAUDRATE,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
            .use_ref_tick = 1,
    };
    /* Install UART driver for interrupt-driven reads and writes */
    ESP_ERROR_CHECK( uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM,
            256, 0, 0, NULL, 0) );
    ESP_ERROR_CHECK( uart_param_config(CONFIG_ESP_CONSOLE_UART_NUM, &uart_config) );
}

void uart_emit(unsigned char uc_val) {
  uart_write_bytes(CONFIG_ESP_CONSOLE_UART_NUM,(char const*)&uc_val,1);
}

g_char uart_getkey() {
  g_char uc_val;
  int val = uart_read_bytes(CONFIG_ESP_CONSOLE_UART_NUM,&uc_val,1,3600000);
  // Echo back on the console
  if(val) {
    uart_emit(uc_val);
    // Translate LF into CR LF
    if(uc_val == '\r') {
      uart_emit('\n');
    }
    return uc_val;
  } else {
    return '\t';
  }
}

g_cell uart_eof() {
  return g_false;
}

unsigned char* source_ptr = source;

g_char source_getkey() {
  if(source_ptr < source_end) {
    #ifdef TRACE_SOURCE
    uart_emit(*source_ptr);
    if(*source_ptr == '\n') {
      uart_emit('\r');
    }
    #endif
    return *source_ptr++;
  } else {
    return '\t';
  }
}

g_cell source_eof() {
  return source_ptr >= source_end;
}

console_io uart_io = {
  .getkey = uart_getkey,
  .emit   = uart_emit,
  .eof    = uart_eof,
};

console_io source_io = {
  .getkey = source_getkey,
  .emit   = uart_emit,
  .eof    = source_eof,
};

void app_main(void)
{
    f_memory p;
    printf("Forth base starting\n");

    initiate_mem(&p);

    unsigned memory_size = memory_end - memory;

    if( bootstrap_mem(&p,memory,memory_size) )
    {
      printf("Unable to initialize.\n");
      // vTaskDelay(10 / portTICK_PERIOD_MS);
      return;
    }

    printf("Memory initialized, starting VM\n");
    // vTaskDelay(10 / portTICK_PERIOD_MS);

    configure_uart();
    run(&p,&source_io);
    printf("Reached end of run(source_io), switching to run(uart_io)\n");
    run(&p,&uart_io);
    printf("Reached end of run(uart_io), restarting\n");
    esp_restart();
}
