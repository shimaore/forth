case 100: opcode(">f")
  push(fp) = pop(sp);
  break;
case 101: opcode("f>")
  push(sp) = pop(fp);
  break;
case 102: opcode("f+")
  f_val = pop(fp);
  top(fp) += f_val;
  break;
case 103: opcode("fnegate")
  top(fp) = -top(fp);
  break;
case 104: opcode("f*")
  f_val = pop(fp);
  top(fp) *= f_val;
  break;
case 105: opcode("f/")
  f_val = pop(fp);
  top(fp) /= f_val;
  break;
case 106: opcode("floor")
  top(fp) = floor(top(fp));
  break;
case 107: opcode("fmod")
  f_val = pop(fp);
  top(fp) = fmod(top(fp),f_val);
  break;
case 108: opcode("fdup")
  f_val = top(fp);
  push(fp) = f_val;
  break;
case 109: opcode("fdrop")
  (void)pop(fp);
  break;
case 110: opcode("fswap")
  f_val = top(fp);
  top(fp) = top2(fp);
  top2(fp) = f_val;
  break;
case 111: opcode("f!")
  val = pop(sp);
  state->p->floats[val] = pop(fp);
  break;
case 112: opcode("f@")
  top(fp) = state->p->floats[pop(sp)];
  break;
case 113: opcode("f-")
  f_val = pop(fp);
  top(fp) -= f_val;
  break;
case 114: opcode("?f0<>")
  f_val = pop(fp);
  push(sp) = forth_boolean(f_val);
  break;
case 115: opcode("?f<")
  f_val = pop(fp);
  push(sp) = forth_boolean(pop(fp) < f_val);
  break;
case 116: opcode("?fisinf")
  f_val = pop(fp);
  push(sp) = forth_boolean(isinf(f_val));
  break;
case 117: opcode("?fisnan")
  f_val = pop(fp);
  push(sp) = forth_boolean(isnan(f_val));
