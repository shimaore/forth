/*
 * (c) 2003-2008 Stephane Alnet
 */
#ifndef __LOADER_H
#define __LOADER_H

#include "base.h"

#include <string.h> /* for size_t */

/* Embed the base Forth machine. */

int bootstrap_mem( f_memory* p, unsigned char const* base_code, size_t base_size );

#endif /* __LOADER_H */
