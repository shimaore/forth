/* Type definitions */

#include "memory.h"

/* ------ Memory Management ------ */

void initiate_mem( f_memory* p )
{
  p->cells = forth_new(g_cell,CELL_MEM_BUCKET_SIZE);
  p->chars = forth_new(g_char,CHAR_MEM_BUCKET_SIZE);
  p->floats = forth_new(g_float,FLOAT_MEM_BUCKET_SIZE);
}
