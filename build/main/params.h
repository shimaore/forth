#ifndef __params_h
#define __params_h

/*
 * Edit this file to specify the different paramateres of the Forth
 * machine.
 *
 * A g_char (Forth CHAR) is the smallest adressable unit in memory.
 * A g_cell (Forth CELL) is used to store memory addresses.
 *
 */

#define ONE_CELL 1

/* MAX_STK: Depth of the stacks (in CELLs). */
/* Note: the SP and RP stacks only store CELLs. */
#define SP_MAX_STK    128
#define RP_MAX_STK    128

#define FP_MAX_STK    32
typedef double g_float;

/* System parameters: choose your system model here. */

  /* Standard 32-bit system, CELL = uint, CHAR = uchar */
  typedef unsigned int   g_cell;
  typedef unsigned char  g_char;
  #define DISPLAY_CELL_HEX    "0x%08x"
  #define DISPLAY_CELL        "%u"
  #define ZERO                ((g_cell)0)
  #define ONE                 ((g_cell)1)

/* MEM_BUCKET_BITS: Defines the size of a memory buffer index.
 */
enum { CELL_MEM_BUCKET_BITS = 13 };
enum { CHAR_MEM_BUCKET_BITS = 13 };
enum { FLOAT_MEM_BUCKET_BITS = 8 };

/* MEM_BUCKET: Size of a single memory buffer (in number of CELLs or CHARs). */
#define CELL_MEM_BUCKET_SIZE    (ONE<<(g_cell)CELL_MEM_BUCKET_BITS)
#define CHAR_MEM_BUCKET_SIZE    (ONE<<(g_cell)CHAR_MEM_BUCKET_BITS)
#define FLOAT_MEM_BUCKET_SIZE   (ONE<<(g_cell)FLOAT_MEM_BUCKET_BITS)

/* Other (computed) definitions. No user serviceable parts within. */

/* g_true:   the TRUE value (all bits ones)
   g_false:  the FALSE value (all bits zeroes)
   neg_mask: mask used to find if a CELL is a positive or a negative value.
*/

#define g_true   ((g_cell)(~ZERO))
#define g_false  ((g_cell)ZERO)

#define neg_mask (ONE << (g_cell)(8*sizeof(g_cell)-1))

#define IP_START      (256)
#define is_opcode(op) (op < IP_START)
#define map_opcode(op)    (op)
#define unmap_opcode(op)  (op)

#endif /* __params_h */
