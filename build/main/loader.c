/*
 * (c) 2003-2008 Stephane Alnet
 */

#undef REQUIRE_BASE_RUNTIME

#include <stdio.h>

/* Type definitions */

#include "loader.h"
#include "memory.h"

/* Called once at startup time to initialize the system memory
  with the base Forth machine. */

/* Load binary bootstrap Forth code from memory. */
int bootstrap_mem( f_memory* p, unsigned char const* base_code, size_t base_size )
{
    g_cell* cell_mem;
    g_char* char_mem;

    if( base_size < 2*sizeof(g_cell) )
    {
      printf("Bootstrap is %u bytes long, which is too short.\n",
                     base_size);
      return 1;
    }

    g_cell* base_code_cell = (g_cell*)base_code;

    /* Initialize Forth machine memory. */
    initiate_mem(p);
    cell_mem = cell_page_zero(p);
    char_mem = char_page_zero(p);

    /* The first two cells are the block sizes. */
    g_cell cell_count = base_code_cell[0];
    g_cell char_count = base_code_cell[1];

    if( base_size != sizeof(g_cell)*(cell_count+2) + sizeof(g_char)*(char_count) )
    {
      printf("Bootstrap is %u bytes long, which doesn't match " DISPLAY_CELL " cells and " DISPLAY_CELL " chars.\n",
                     base_size,cell_count,char_count);
      return 1;
    }

    memcpy(cell_mem,base_code+sizeof(g_cell)*2,sizeof(g_cell)*cell_count);
    memcpy(char_mem,base_code+sizeof(g_cell)*(cell_count+2),sizeof(g_char)*char_count);

    printf("Loaded " DISPLAY_CELL " cells, " DISPLAY_CELL " chars\n",cell_count,char_count);

    return 0;
}
