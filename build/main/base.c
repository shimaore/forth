/*
 * (c) 2003-2020 Stephane Alnet
 */

#include <string.h>
/* Type definitions */

#include "base.h"
#include "memory.h"

#include <string.h>
#include <math.h>
#define opcode(o)

#define forth_boolean(v) ((v) ? g_true : g_false)

/* -------- Model for a standard machine. ---------- */

typedef struct thread_state {

  int    running;
  g_cell ip;

  f_memory* p;
  console_io* console;

  g_cell *sp;
  g_cell sp_start[SP_MAX_STK];                /* Data Stack */

  g_cell *rp;
  g_cell rp_start[RP_MAX_STK];                /* Return Stack */

  g_float *fp;
  g_float fp_start[FP_MAX_STK];               /* Float Stack */

} thread_state;

inline
void initialize_thread( thread_state* t, f_memory* p, console_io* console )
{
    t->ip = IP_START;
    /* The stacks start at the end and grow downward */
    t->sp = t->sp_start+SP_MAX_STK;
    t->rp = t->rp_start+RP_MAX_STK;
    t->fp = t->fp_start+FP_MAX_STK;
    t->running = 1;
    t->p = p;
    t->console = console;
}

void run_one_step( thread_state* state )
{

        #define sp   (state->sp)
        #define rp   (state->rp)
        #define ip   (state->ip)
        #define peek(a) __peek(state->p,a)
        #define cpeek(a) __cpeek(state->p,a)
        #define poke(a,x) __poke(state->p,a,x)
        #define cpoke(a,x) __cpoke(state->p,a,x)

        #define top(s)    *(s)
        #define top2(s)   *(s+1)
        #define pop(s)    *(s++)
        #define push(s)   *(--s)

        g_cell op, val;
        #define fp    (state->fp)
        g_float f_val;

        op = peek(ip);

        ip ++;

        if( is_opcode(op) )
        {
            switch( map_opcode(op) )
            {
            /* Note: This is not the smallest core possible.
             *       BRANCH, 0BRANCH and LIT, for example, can be
             *       implemented in FORTH itself.
             *       However the performances tend to be unacceptable
             *       because these are used very often.
             */

            /*
             * Do not change the format of the 'case' lines, they are used to
             * automatically generate opcodes.f and opcodes.t.
             */

            case  0: opcode("exit")
              ip     = pop(rp);
              break;

            case  1: opcode(">r")
              push(rp) = pop(sp);
              break;
            case  2: opcode("r@")
              push(sp) = top(rp);
              break;
            case  3: opcode("r>")
              push(sp) = pop(rp);
              break;

            case  4: opcode("dup")
              val = top(sp);
              push(sp) = val;
              break;
            case  5: opcode("drop")
              (void)pop(sp);
              break;
            case  6: opcode("rdrop")
              (void)pop(rp);
              break;
            case  7: opcode("nip")
              val = pop(sp);
              top(sp) = val;
            break;

            // take 2, return 2
            /* The following is a complicated way to implement "add with carry"
             * in C. Semantically this can also be thought as returning the
             * (double) sum of two unsigned CELLs.
             */
            case  8: opcode("+c")
              val  = top(sp) + top2(sp);
              top(sp) = ((val < top(sp)) || (val < top2(sp))) ? 1 : 0;
              top2(sp) = val;
              break;
            case  9: opcode("swap")
              val = top(sp);
              top(sp) = top2(sp);
              top2(sp) = val;
              break;

            // take 2, return 3
            case 10: opcode("over")
              val = top2(sp);
              push(sp) = val;
              break;

            // take 2, consumes 1, return 1
            // not required
            case 11: opcode("u<")
              val = pop(sp);
              top(sp) = forth_boolean(top(sp) < val);
              break;

            case 12: opcode("+")
              val = pop(sp);
              top(sp) += val;
              break;
            case 13: opcode("and")
              val = pop(sp);
              top(sp) &= val;
              break;
            case 14: opcode("or")
              val = pop(sp);
              top(sp) |= val;
              break;
            // not required
            case 15: opcode("xor")
              val = pop(sp);
              top(sp) ^= val;
              break;

            // in-place (take 1, return 1)
            /* These operations are the ones that most benefit from having
               sp_top as a register.
            */
            case 16: opcode("1+")
              top(sp) += 1;
              break;
            case 17: opcode("0=")
              top(sp) = forth_boolean(top(sp) == 0);
              break;
            case 18: opcode("negate")
              top(sp) = -(top(sp));
              break;
            case 19: opcode("invert")
              top(sp) = ~(top(sp));
              break;
            case 20: opcode("-")
              val = pop(sp);
              top(sp) -= val;
              break;

            case 21: opcode("2/")
              top(sp) >>= 1;
              break;

            case 22: opcode("1-")
              top(sp) -= 1;
              break;

            case 23: // opcode("execute")
              push(rp) = ip;
              ip = pop(sp);
              break;

            case 24: opcode("c@")
              top(sp) = cpeek(top(sp));
              break;
            case 25: opcode("@")
              top(sp) = peek(top(sp));
              break;

            // take 2, consumes 2, return 0
            case 26: opcode("c!")
              val = pop(sp);
              cpoke(val,pop(sp));
              break;
            case 27: opcode("!")
              val = pop(sp);
              poke(val,pop(sp));
              break;

            // take 0, return 1
            case 28: opcode("lit")
              push(sp) = peek(ip);
              ip ++;
              break;

            // take 1, return 0
            case 29: opcode("0branch")
              ip = (pop(sp) == 0)
                    ? peek(ip)
                    : ip+1;
              break;

            // take 0, return 0
            case 30: opcode("branch")
              ip = peek(ip);
              break;

            case 31: opcode("bye")
              state->running = 0;
              break;
            case 32: opcode("rp!")
              rp = state->rp_start+RP_MAX_STK;
              break;
            case 33: opcode("sp!")
              sp = state->sp_start+SP_MAX_STK;
              break;

            // take 0, return 1 (I/O)
            case 34: opcode("get-key")
              push(sp) = state->console->getkey();
              break;

            // take 1, return 0 (I/O)
            case 35: opcode("emit")
              state->console->emit(pop(sp));
              break;

            // take 0, return 1 (I/O)
            case 36: opcode("eof")
              push(sp) = state->console->eof();
              break;

            case 37: opcode("?interactive")
              push(sp) = g_true;
              break;

            case 38: opcode("u*")
              // This works because g_cell is always unsigned.
              val = pop(sp);
              top(sp) *= val;
              break;

            case 39: opcode("u/mod")
              // This works because g_cell is always unsigned.
              val = top(sp);
              top(sp) = top2(sp)/val;
              top2(sp) %= val;
              break;

            case 40: opcode("cmove")
              {
                g_cell len = pop(sp);
                g_cell dst = pop(sp);
                g_cell src = pop(sp);
                __cmove(state->p,dst,src,len);
              }
              break;

            case 41: opcode("move")
              {
                g_cell len = pop(sp);
                g_cell dst = pop(sp);
                g_cell src = pop(sp);
                __move(state->p,dst,src,len);
              }
              break;

            case 42: opcode("true")
              push(sp) = g_true;
              break;

            case 43: opcode("false")
              push(sp) = g_false;
              break;

            case 44: opcode("2*")
              top(sp) *= 2;
              break;

            case 45: opcode("2dup")
              val = top2(sp);
              push(sp) = val;
              val = top2(sp);
              push(sp) = val;
              break;

            case 46: opcode("2drop");
              (void)pop(sp);
              (void)pop(sp);
              break;

            case 47: opcode("+!");
              val = pop(sp);
              poke(val,peek(val)+pop(sp));
              break;

            case 48: opcode("0<");
              val = top(sp);
              top(sp) = forth_boolean(val & neg_mask);
              break;
              break;

              #include "float.h"

            default:
              /* Invalid opcode. */
              break;
            }
        }
        else /* sub-call */
        {
            push(rp) = ip;
            ip = op;
        }
}

/* Main loop for a single-threaded system. */
int run( f_memory* p, console_io* console )
{
    thread_state t;
    initialize_thread(&t,p,console);
    /* run */
    while(t.running > 0)
    {
        run_one_step(&t);
    } /* while */
    return t.running;
} /* run() */

/* --- End of file base.c --- */
