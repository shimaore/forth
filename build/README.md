To reconfigure, run from source:

```shell
docker run --rm -it -v ${PWD}/build:/project -w /project espressif/idf idf.py menuconfig
```
