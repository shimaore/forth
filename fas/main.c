/*
 * fas.c - the Forth assembler
 * (c) 2009 Stephane Alnet - GPL3+
 */

#include "fas.h"
#include "../base/memory.h"

#include <stdlib.h> /* fprintf, exit */

#define debug 0

#ifdef USE_FLASH
uint8_t flash[0x80000];

g_cell here = ADDR_FLASH;
#else
f_memory block;

g_cell cell_here = IP_START;
#endif

#ifndef COMMON_MEMORY
g_cell char_here = 0;
#endif

g_cell latest = 0;

#include <stdio.h>

void dump()
{
#ifdef USE_FLASH
  fwrite(flash,1,here-ADDR_FLASH,stdout);
#else
  fwrite(&cell_here,sizeof(cell_here),1,stdout);
  fwrite(&char_here,sizeof(char_here),1,stdout);
  fwrite(cell_page_zero(&block),sizeof(g_cell),cell_here,stdout);
  fwrite(char_page_zero(&block),sizeof(g_char),char_here,stdout);
#endif
}

void show_missing_symbols();

/* Called by yyparse on error.  */
void yyerror (char const *s)
{
 fprintf (stderr, "yyerror: '%s'\n", s);
}

/* main */

int main(void)
{
#ifdef FORTH_HAS_GC
  GC_INIT();
#endif
#ifndef USE_FLASH
  initiate_mem(&block);
#endif
  if(yyparse()) {
    fprintf (stderr, "yyparse failed\n");
    return 1;
  }
  create_name("last_word",latest);
  create_name("last_cell",cell_here);
  create_name("last_char",char_here);
  show_missing_symbols();
  dump();
  return 0;
}

/* Output block management */

void set_cell(g_cell addr, g_cell value)
{
  if(debug) fprintf(stderr, "      set_cell(" DISPLAY_CELL_HEX ") to " DISPLAY_CELL_HEX "\n",addr,value);
#ifdef USE_FLASH
  addr -= ADDR_FLASH;
  flash[addr+0] = (value >> 16) & 0xff;
  flash[addr+1] = (value >>  8) & 0xff;
  flash[addr+2] = (value >>  0) & 0xff;
#else
  __poke(&block,addr,value);
#endif
}

void set_char(g_cell addr, g_char value)
{
  if(debug) fprintf(stderr, "      set_char(" DISPLAY_CELL_HEX ") to %02x (%c)\n",addr,value,value);
#ifdef USE_FLASH
  addr -= ADDR_FLASH;
  flash[addr] = value;
#else
  __cpoke(&block,addr,value);
#endif
}


typedef struct Cell Cell;
struct Cell {
  g_cell    value;
  int       line;
  Cell*     next;
};

Cell* new_cell(g_cell car, g_cell line, Cell* cdr)
{
  Cell* node = gc_malloc(sizeof(Cell));
  node->value = car;
  node ->line = line;
  node->next = cdr;
  return node;
}

typedef struct Atom Atom;
struct Atom {
  UT_hash_handle  hh;
  short           defined;
  g_cell          value;
  Cell*           backref_list;
  char            name[];
};

Atom* atoms = NULL;

Atom* new_atom(char const* name)
{
  unsigned len = strlen(name)+1;
  Atom* atom = gc_malloc(sizeof(Atom)+len);
  atom->defined = 0;
  atom->value = -1;
  atom->backref_list = NULL;
  memcpy(atom->name,name,len);
  HASH_ADD_STR(atoms,name,atom);
  return atom;
}

void show_missing_symbols()
{
  Atom* this;
  for( this = atoms; this != NULL; this = this->hh.next )
  {
    if(!this->defined && this->backref_list)
    {
      Cell* cur = this->backref_list;
      while(cur) {
        fprintf(stderr,"Undefined symbol `%s` at line %d\n",this->name,cur->line);
        cur = cur->next;
      }
      exit(1);
    }
  }
}

void compile_value(g_cell value)
{
  if(debug) fprintf(stderr, "    compile_value(" DISPLAY_CELL_HEX ") at " DISPLAY_CELL_HEX "\n",value,cell_here);
  set_cell(cell_here,value);
  cell_here += ONE_CELL;
}

void compile_word(char const* name)
{
  if(debug) fprintf(stderr, "  compile_word(%s)\n",name);
  Atom* atom = NULL;
  HASH_FIND_STR(atoms,name,atom);
  if(!atom)
  {
    atom = new_atom(name);
  }

  if(atom->defined)
  {
    compile_value(atom->value);
  }
  else
  {
    if(debug) fprintf(stderr, "    postponing %s at " DISPLAY_CELL_HEX "\n",name,cell_here);
    atom->backref_list = new_cell(cell_here,yylineno,atom->backref_list);
    cell_here += ONE_CELL;
  }
}

g_cell get_value(char const* name)
{
  Atom* atom = NULL;
  HASH_FIND_STR(atoms,name,atom);
  if(atom && atom->defined)
  {
    return atom->value;
  }
  fprintf(stderr,"Undefined value: %s\n",name);
  exit(1);
}

void resolve_backrefs(Cell* backref, g_cell value)
{
  if(backref)
  {
    fprintf(stderr,"    .. from line %d\n",backref->line);
    set_cell(backref->value,value);
    resolve_backrefs(backref->next,value);
    backref->next = NULL;
  }
}

void create_name(char const* name, g_cell value)
{
  if(debug) fprintf(stderr, "create_name(%s," DISPLAY_CELL_HEX ")\n",name,value);
  Atom* atom = NULL;
  HASH_FIND_STR(atoms,name,atom);
  if(atom)
  {
    if(atom->defined)
    {
      fprintf(stderr,"Duplicate name: %s\n",name);
      exit(1);
    }
    else
    {
      fprintf(stderr,"    resolving backrefs for %s\n",name);
      resolve_backrefs(atom->backref_list,value);
      atom->backref_list = NULL;
    }
  }
  else
  {
    atom = new_atom(name);
  }
  atom->defined = 1;
  atom->value = value;
}

void compile_header(char const* name, char const* cpfa)
{
  if(debug) fprintf(stderr, "compile_header(%s)\n",name);

  g_cell name_pos = char_here;

  unsigned len = strlen(name);

  // Save the name
  unsigned i;
  for( i = 0; i < len; i++ )
  {
    set_char(char_here+i,name[i]);
  }
  char_here += len;


  // NFA
  g_cell new_latest = cell_here;
  if(debug) fprintf(stderr, "  NFA: len\n");
  compile_value(len);       // len
  if(debug) fprintf(stderr, "  NFA: name\n");
  compile_value(name_pos); // addr(c)

  // LFA
  if(debug) fprintf(stderr, "  LFA\n");
  compile_value(latest);
  latest = new_latest;

  // CPFA
  if(debug) fprintf(stderr, "  CPFA\n");
  compile_word(cpfa); // other cpfa

  // PFA
  if(debug) fprintf(stderr, "  PFA\n");
  create_name(name,cell_here);
}

void FAIL_MALLOC_FAILED()
{
  fprintf(stderr,"gc_malloc failed\n");
  exit(2);
}

#ifdef FORTH_HAS_GC
void* gc_malloc( size_t sz )
{
  void* new_ptr = GC_MALLOC(sz);
  if(!new_ptr) FAIL_MALLOC_FAILED();
  return new_ptr;
}
#else

void* gc_malloc( size_t sz )
{
  void* new_ptr = malloc(sz);
  if(!new_ptr) FAIL_MALLOC_FAILED();
  return new_ptr;
}
#endif /* FORTH_HAS_GC */
