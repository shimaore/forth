%{
  #include "../base/params.h"
  #include <stdlib.h> /* fprintf, exit */

  #define WORD   1

  struct {
    g_cell  i;
    char*   s;
  } yylval;

%}

%x comment

DIGITS  [0-9]+
BLANK   [ \t\r\n]
BLANKS  {BLANK}+
WORD    [^ \t\r\n]+

%%

-?{DIGITS}  {
              yylval.i = atoll(yytext);
              yylval.i = unmap_opcode(yylval.i);
            }

{WORD}    {
            yylval.s = yytext;
            return WORD;
          }

{BLANKS}            /* ignore */

%%

int yywrap() { return 1; }

int usage( char const* my_name )
{
  fprintf(stderr,"%s [-i] [-b] <format>\n",my_name);
  return 1;
}

enum { BIN_MAX = sizeof(g_cell)*8+1 };

int main( int argc, char const* argv[] )
{
  short opcode_first = 0;
  short opcode_binary = 0;
  char const* format;
  char const* my_name;

  my_name = *argv;
  argv++;

  if(*argv && (*argv)[0] == '-' && (*argv)[1] == 'i' && (*argv)[2] == '\0') {
    opcode_first = 1;
    argv++;
  }

  if(*argv && (*argv)[0] == '-' && (*argv)[1] == 'b' && (*argv)[2] == '\0') {
    opcode_binary = 1;
    argv++;
  }

  if(*argv) {
    format = *argv;
    argv++;
  } else {
    return usage(my_name);
  }

  if(*argv) {
    return usage(my_name);
  }

  while(1) {
    switch(yylex()) {
      case WORD:
        {
          char buffer[BIN_MAX];
          char *result = buffer;
          if(opcode_binary) {
            result = buffer + BIN_MAX;
            *result = '\0';
            while(result > buffer && yylval.i) {
              *(--result) = (yylval.i & 1) + '0';
              yylval.i >>= 1;
            }
          } else {
            snprintf(result,BIN_MAX,DISPLAY_CELL,yylval.i);
          }
          if(opcode_first) {
            fprintf(stdout,format,result,yylval.s);
          } else {
            fprintf(stdout,format,yylval.s,result);
          }
          fprintf(stdout,"\n");
        }
        break;
      default:
        return 0;
    }
  }
}
