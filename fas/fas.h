#ifndef FAS_H
#define FAS_H

/* uthash: http://uthash.sourceforge.net/ */
// #include <uthash.h>
#include "./uthash/uthash.h"

void* gc_malloc( size_t sz );

#ifdef FORTH_HAS_GC
/* boehmgc */
#include <gc.h>

/* undefine the defaults */
#undef uthash_bkt_malloc
#undef uthash_bkt_free
#undef uthash_tbl_malloc
#undef uthash_tbl_free

/* re-define, specifying alternate functions */
/* for UT_hash_bucket */
#define uthash_bkt_malloc(sz) gc_malloc(sz)
#define uthash_bkt_free(ptr)  /* GC_FREE(ptr) */
/* for UT_hash_table  */
#define uthash_tbl_malloc(sz) gc_malloc(sz)
#define uthash_tbl_free(ptr)  /* GC_FREE(ptr) */

#endif /* FORTH_HAS_GC */

#include "../base/params.h"

int yylex(void);
int yylineno;
void yyerror(char const*);

void create_name(char const* name, g_cell value);
void compile_header(char const* name, char const* cpfa);
void compile_word(char const* name);
g_cell get_value(char const* name);

#ifdef COMMON_MEMORY
/* Use the same memory blocks for CELL and CHAR memories */
g_cell here;
#define char_here here
#define cell_here here
#else
/* Use independent memory blocks for CELL vs CHAR memories */
g_cell cell_here;
g_cell char_here;
#endif

void FAIL_MALLOC_FAILED();
int yyparse();

#endif
