\ +C: u1 u2 -- sum carry
: +c
  2dup + >r
  \ If the result is lower than either operands then overflow happened.
  r@ u>
  swap
  r@ u>
  or
  if 1 else 0 then
  r> swap
;

