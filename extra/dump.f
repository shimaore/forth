\ ((
\ 
\   dump is used to generate the binary files which are used to initialize the
\   forth memory during bootstrap. It simply prints out the content of the
\   memory to stdout.
\ 
\   : dump 0 BEGIN DUP HERE U< WHILE DUP C@ EMIT CHAR+ REPEAT ;
\ 
\ ))
\ 
\ : dump
\     LIT 0
\ dump_mem_while: \ BEGIN
\     DUP HERE U<
\     0BRANCH exit_now \ WHILE
\     DUP C@ EMIT  CHAR+
\     BRANCH dump_mem_while \ REPEAT
\ 

: dump
       here emit_cell
  char-here emit_cell
       here >r 0 begin dup r@ u< while dup  @ emit_cell cell+ repeat drop rdrop
       0 char-here type
  ;

