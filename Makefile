# Makefile for myforth
# (c) 2003-2008 Stephane Alnet

## Location where to install the forth program
#INSTALL_BIN_PATH =/usr/local/bin
INSTALL_BIN_PATH =${HOME}/Applications
## Location for the bootfiles
INSTALL_BASE_PATH=${INSTALL_BIN_PATH}

#  Compile without U* and U/MOD in base.c
#  (i.e. -DFORTH_NO_MULDIV)
# FORTH_LIBS = base/base.f \
#              boot/char.f boot/comment.f \
#              boot/branch.f boot/does.f \
#              boot/misc.f boot/noname.f boot/value.f \
#              boot/umul.f boot/base.f \
#              boot/string.f \
#              run/run.f run/interpreter.f \
#              boot/udiv.f boot/numprint.f extra/dump.f \
#              boot/case.f

FORTH_LIBS = base/base.f \
             base/float.f \
             boot/char.f boot/comment.f \
             boot/branch.f boot/does.f \
             boot/misc.f boot/noname.f boot/value.f \
             boot/arithm.f \
             boot/base.f \
             boot/string.f \
             run/run.f run/interpreter.f \
             boot/numprint.f extra/dump.f \
             boot/case.f

all: base/forth

apk:
	apk --update add make gcc gc-dev flex bison musl-dev

install: all
	cp base/forth ${INSTALL_BIN_PATH}/forth

## Generating stage 1 (bootstrap) code ##
bin/forth.base.1: base/base.t bootstrap.t
	cd fas && make fas
	mkdir -p bin/
	mv fas/fas bin/fas
	cat $^ | bin/fas > $@

## Generating stage 2 (boot and base libs) code ##
base/source: ${FORTH_LIBS}
	cat $^ > $@
base/memory_base_code: bin/boot-forth bin/forth.base.1 base/source
	echo dump | $^ - > $@

## Generating esp32 files
build/main/source: build/main/base.f build/main/float.f \
             boot/char.f boot/comment.f \
             boot/branch.f boot/does.f \
             boot/misc.f boot/noname.f boot/value.f \
             boot/arithm.f \
             boot/base.f \
             boot/string.f \
             run/interpreter.f \
             boot/numprint.f \
             boot/case.f
	cat $^ > $@
build/main/memory: bin/forth.base.1
	cat $^ > $@

%.list: %.c
	perl -ne '/^ *case +([0-9]+): *opcode\("([^"]+)"\)/ and { print "$$1 $$2\n" }' $< > $@
%.list: %.h
	perl -ne '/^ *case +([0-9]+): *opcode\("([^"]+)"\)/ and { print "$$1 $$2\n" }' $< > $@

bin/opcodes:
	cd fas && make opcodes
	mkdir -p bin
	mv fas/opcodes bin/opcodes

%.f: %.list
	make bin/opcodes
	bin/opcodes -i '%s :opcode %s' < $< > $@

%.t: %.list
	make bin/opcodes
	bin/opcodes    '.inline %s %s' < $< > $@

bin/boot-forth:
	cd base && make boot-forth
	mkdir -p bin/
	mv base/boot-forth bin/boot-forth

base/forth: base/memory_base_code
	cd base && make forth
	mv base/forth bin/forth

bin/get-params:
	cd base && make get-params
	mkdir -p bin/
	mv base/get-params bin/get-params

clean:
	# Remove boot
	-rm bin/boot-forth bin/fas bin/opcodes
	# Remove targets
	-rm runtime.f bin/opcodes.f params.pl bin/forth.base log.out
	-rm base/memory_base_code.*
	-rm bin/float.f bin/float.list
	-rm bin/forth base/memory_base_code
	# Remove non-targets
	-rm bin/get-params op_min.h
	# Remove other misc.
	-rm *.o forth.exe base base.exe get-params.exe
	-rm bootstrap.code bin/opcodes.t bin/types.t bin/opcodes.list base/base.t base/base.list base/base.f base/base.t base/float.f base/source
	-rm bin/forth.base bin/forth.base.[1-4]
	cd base && make clean
	cd fas  && make clean

war:
	@echo 'Not love?'

esp32:
	# docker run -v ${PWD}:/project -w /project       espressif/idf make build/main/source
	# docker run -v ${PWD}:/project -w /project       espressif/idf make build/main/memory
	make build/main/source
	make build/main/memory
	docker run --rm -v ${PWD}:/project -w /project/build espressif/idf make
esp32-menuconfig:
	docker run --rm -it -v ${PWD}:/project -w /project/build espressif/idf make menuconfig
esp32-flash: esp32
	esptool --chip esp32 --port "/dev/ttyUSB0" --before "default_reset" --after "hard_reset" \
		write_flash -z --flash_mode "dio" --flash_freq "40m" --flash_size detect \
		0x1000  build/build/bootloader/bootloader.bin \
		0x10000 build/build/forth.bin \
		0x8000  build/build/partitions.bin
esp32-console:
	minicom -o -D /dev/ttyUSB0
