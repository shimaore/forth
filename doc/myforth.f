
\ (BRANCH,LIT) MARK .. FORWARD (absolute)
: MARK      HERE 0 , ;
: FORWARD   HERE SWAP ! ;

\ HERE .. (BRANCH,LIT) BACK (absolute)
: BACK      , ;

\ When MARK or BACK are preceded with BRANCH or 0BRANCH,
\ the possible combinations are the following ones:
\ Unconditional forward branch:  AHEAD
\ Conditional forward branch:    IF
\ Unconditional backward branch: AGAIN
\ Conditional backward branch:   UNTIL

\ Note that THEN is an immediate version of FORWARD
\      and BEGIN is an immediate version of HERE
\ THEN is a target for AHEAD and IF
\ BEGIN a destination for AGAIN and UNTIL

\ All other constructs are built upon these six words.
\ (Except for sequencing, which occurs naturally.)

\ Use        AHEAD (not executed) .. THEN (continues here)
\ (i.e. SKIP/JUMP ahead)
: AHEAD  POSTPONE BRANCH MARK ; IMMEDIATE

: IF     POSTPONE 0BRANCH MARK ; IMMEDIATE
: THEN   FORWARD ; IMMEDIATE
: ELSE   >R POSTPONE AHEAD R> POSTPONE THEN ; IMMEDIATE

: BEGIN  HERE ; IMMEDIATE
: AGAIN  POSTPONE BRANCH BACK ; IMMEDIATE
: UNTIL  POSTPONE 0BRANCH BACK ; IMMEDIATE
: WHILE  >R POSTPONE IF R> ; IMMEDIATE
: REPEAT POSTPONE AGAIN POSTPONE THEN ; IMMEDIATE



\ CREATE .. DOES>

\ In the case of a creation word that does not have a DOES> section, for example:
\ : var CREATE 1 CELL ALLOT ;   <- regularly compiled code
\ then the created word looks like this:
\    NFA "name"
\    LFA
\    PFA (CREATE)
\    ..data area

\ Now, let's consider the following definition:
\ : constant CREATE , DOES> @ ;
\ The creating word looks like this once compiled:
\ : constant CREATE ,         <- regularly compiled code (creating word)
\            (DOES>)          <- when executed, (DOES>) does the magic with the PFA of the
\                                created word, and exits "constant" (very important since
\                                we do not want to execute what follows at the time the word is created).
\     (*)    R>               <- here starts the code executed by the created words
\            @ ;              <- regularly compiled code
\ and the created word looks like this:
\    NFA   "name"
\    LFA
\    PFA   call-to-(*)  (above)
\    ..data area
\ To summarize, DOES> creates a second, distinct PFA area that is then used
\ by created words as their PFAs (modulo an extra call which allows to
\ get the data-address on the RP stack!).

\ (CREATE) ( RP: return data-addr -- ) ( SP: -- data-addr )
: (CREATE) R> ;

\ CREATE ( -- )
: CREATE
  HEADER
  POSTPONE (CREATE)
  \ The data section follows (CREATE) in the created word.
  \ (In other word the PFA of a word created by CREATE contains a single CELL call.)
  \ finishes the created word
  LATEST!
  ;

\ Historical note: CREATE used to not contain LATEST!, which was done later,
\ in DOES> . However this clearly fails with :opcode, which uses IMMEDIATE,
\ since IMMEDIATE needs LATEST to point to the word actually defined by
\ CREATE, and IMMEDIATE has to occur before DOES>. .


\ >BODY ( xt -- data-addr )
\ returns a pointer to the data section of a word created by CREATE
: >BODY      CELL+ ;

: (DOES>)
  \ replaces (CREATE) in the created word by the word that follows (DOES>) in the creating word.
  R> PFA @ !
  0 PFA !
  \ and exits the creating word
  ;

: DOES>
  \ (DOES) will replace (CREATE) in the created word by a call to
  \ R> (postponed below in the definition of the creating word) 
  \ and subsequent code in the creating word.
  \ It is the last (executed) word of the creating word.
  POSTPONE (DOES>)
  \ The return location would then be the location of the data in the
  \ created word; we simply use it as data-addr, as in (CREATE).
  \ The difference with (CREATE) is that (CREATE) does R> EXIT at once
  \ while here we do R> at this point, and EXIT occurs later
  \ (at the end of the code of the creating word, actually).
  POSTPONE R>
  ; IMMEDIATE


