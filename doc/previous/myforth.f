\ myforth.f
\ (c) 2003,2004 Stephane Alnet

: 2*     DUP + ;
: 2DUP   OVER OVER ;
: 2DROP  NIP DROP ;

: ROT    >R SWAP R> SWAP ;
: UNROT  SWAP >R SWAP R> ;
: TUCK   SWAP OVER ;
\ : NIP    SWAP DROP ;
: +!     TUCK @ + SWAP ! ;

: STATE  [ _STATE  [LITERAL] ] ;
: LATEST [ _LATEST [LITERAL] ] @ ;

: IMMEDIATE LATEST DUP C@ [ imm [LITERAL] ] OR SWAP C! ;

: TRUE   [ true  [LITERAL] ] ;
: FALSE  [ 0     [LITERAL] ] ;

: 0      [  0 [LITERAL] ] ;
: 1      [  1 [LITERAL] ] ;
: -1     [ -1 [LITERAL] ] ;

: 0<>    0= INVERT ;
: -      NEGATE + ;
: 1-     -1 + ;

: DP     [ _DP     [LITERAL] ] ;
: HERE   DP @ ;

: ALIGNED  DUP ALIGNMENT + ;
: ALIGN    HERE ALIGNED DP ! ;

: PAD    HERE [ 256 [LITERAL] ] + ;
: ALLOT  DP +! ;
: ,      HERE !  1 CELLS ALLOT ;

: COMPILE,  ALIGN , ;

: [LITERAL]  POSTPONE LIT , ;
: LITERAL    [LITERAL] ; IMMEDIATE

: [   0 STATE C! ; IMMEDIATE
: ]   [ imm [LITERAL] ] STATE C! ;

: 0<  [ neg_mask [LITERAL] ] AND 0<> ;
: <   - 0< ;
: =   - 0= ;
: <>  = INVERT ;
: >   SWAP < ;


\ (BRANCH,LIT) MARK .. FORWARD (absolute)
: MARK     HERE 0 , ;
: FORWARD  HERE SWAP ! ;

\ HERE .. (BRANCH,LIT) BACK (absolute)
: BACK   , ;

\ AHEAD (not executed) .. THEN (continues here)
\ (i.e. skip/jump ahead)
: AHEAD  POSTPONE BRANCH MARK ; IMMEDIATE

: IF     POSTPONE 0BRANCH MARK ; IMMEDIATE
: THEN   FORWARD ; IMMEDIATE
: ELSE   >R POSTPONE AHEAD R> POSTPONE THEN ; IMMEDIATE

: BEGIN  HERE ; IMMEDIATE
: AGAIN  POSTPONE BRANCH BACK ; IMMEDIATE
: UNTIL  POSTPONE 0BRANCH BACK ; IMMEDIATE
: WHILE  >R POSTPONE IF R> ; IMMEDIATE
: REPEAT POSTPONE AGAIN POSTPONE THEN ; IMMEDIATE


: ?DUP   DUP IF DUP THEN ;

: CHAR+  1+ ;
: COUNT  DUP CHAR+ SWAP C@ ;

\ CMOVE : addr-src addr-dest u --
: CMOVE
  \ CHARS
  BEGIN
    ?DUP
  WHILE
     1- >R
    2DUP SWAP C@ SWAP C!
    CHAR+ SWAP CHAR+ SWAP
    R> 
  REPEAT
  2DROP ;

\ U*: u1 u2 -- u3
: U*
  2DUP > IF SWAP THEN
  >R
  0 SWAP
  BEGIN ?DUP WHILE
    DUP 1 AND
    IF SWAP R@ + SWAP THEN
    R> 2* >R
    2/
  REPEAT
  RDROP ;

\ ENCLOSE= : addr1 c "cccX" -- addr2
: ENCLOSE=
  >R
  BEGIN
    DUP C@
    DUP IF R@ = THEN
  WHILE CHAR+ REPEAT
  RDROP
  ;

\ ENCLOSE<> : addr1 c "XXXc" -- addr2
: ENCLOSE<>
  >R
  BEGIN
    DUP C@
    DUP IF R@ <> THEN
  WHILE CHAR+ REPEAT
  RDROP ;

\ addr c -- addr n1 n2 n3
: ENCLOSE
  >R DUP
  DUP
  R@ ENCLOSE=
  2DUP SWAP - UNROT
  R@ ENCLOSE<>
  2DUP SWAP - UNROT
  R> ENCLOSE=
  SWAP -
  ;

\ WORD : char "<char*>ccc<char*>" -- caddr | 0
: WORD
  \ char --
  [ _TIB [LITERAL] ] @ [ >IN [LITERAL] ] @ + SWAP ENCLOSE
  \ addr n1 n2 n3
  [ >IN [LITERAL] ] +!
  \ addr n1 n2
  OVER -
  \ addr n1 length
  >R
  \ addr n1
  + 1-
  \ addr-start
  R@
  IF
    R> OVER C!
  ELSE
    \ addr-start
    DROP R>
  THEN
  ;

\ WORD, : caddr --
: WORD,
  COUNT DUP >R HERE C!
  HERE CHAR+ R@ CMOVE
  R> CHAR+ ALLOT ALIGN ;

: ?IMMEDIATE
  C@ [ imm [LITERAL] ] AND
  IF 1 ELSE -1 THEN ;

: S=
  ROT
  DUP >R
  =
  IF
    BEGIN
      R@
      IF 2DUP C@ SWAP C@ =
      ELSE FALSE
      THEN
    WHILE
      R> 1- >R
      CHAR+ SWAP CHAR+ SWAP
    REPEAT
    2DROP R> 0=
  ELSE
    RDROP 2DROP FALSE
  THEN ;

: EXECUTE   >R ;
: ?EXECUTE  ?DUP IF EXECUTE THEN ;

: (NFA-COUNT)  COUNT [ sysmask [LITERAL] ] AND ;
: NFA=         >R (NFA-COUNT) R> (NFA-COUNT) S= ;

: NFA  [ _NFA [LITERAL] ] ;
: PFA  [ _PFA [LITERAL] ] ;

\ : NFA>LFA (NFA-COUNT) CHARS + ALIGNED ;
\ PFA@ == xt

: NFA>LFA  (NFA-COUNT) + ALIGNED ;
: LFA,     ALIGN LATEST , ;
: NFA>PFA  NFA>LFA CELL+ ;
: PFA,     ALIGN HERE PFA !  ;

: (FIND-NFA)
  BEGIN
    DUP
    IF 2DUP NFA= INVERT
    ELSE FALSE
    THEN
  WHILE
    NFA>LFA @
  REPEAT
  ;

: FIND
  LATEST (FIND-NFA)
  DUP
  IF NIP
    DUP NFA>PFA SWAP ?IMMEDIATE
  THEN
  ;

: BL  [ 32 [LITERAL] ] ;

: (FIND)   BL WORD DUP IF FIND THEN ;

: POSTPONE
  (FIND)
  0<
  IF
    POSTPONE LITERAL POSTPONE COMPILE,
  ELSE
    COMPILE,
  THEN
  ; IMMEDIATE

: [COMPILE] (FIND) DROP COMPILE, ; IMMEDIATE

\ should error if WORD returns 0
: CHAR    BL WORD COUNT DROP C@ ;
: [CHAR]  CHAR [LITERAL] ; IMMEDIATE

: LATEST!  NFA @ ?DUP IF [ _LATEST [LITERAL] ] ! THEN ;


\ :NONAME ( -- xt )
: :NONAME
  0 NFA !
  PFA,
  HERE
  ]
  ;

: HEADER
  HERE NFA !
  BL WORD
  \ should error if 0
  WORD,
  LFA,
  PFA,
  ;

: :
  HEADER 
  ]
  ;

: ;
  POSTPONE EXIT
  LATEST!
  [COMPILE] [
  ALIGN
  0 PFA !
  ; IMMEDIATE

: (CREATE) R> ;

: CREATE
  HEADER
  POSTPONE (CREATE)
  ;

\ >BODY ( xt -- data-addr )
\ returns a pointer to the data section of a word created by CREATE
: >BODY  CELL+ ;

: (DOES>)
  R> PFA @ !
  LATEST!
  0 PFA !
  ;

: DOES>
  POSTPONE (DOES>)
  POSTPONE R>
  ; IMMEDIATE


\ These versions only deal with base 10.
\ >DIGIT : c -- u true | false
: >DIGIT
  [CHAR] 0 -
  DUP  0< INVERT
  OVER [ 10 [LITERAL] ] <
  AND
  \ u flag
  DUP INVERT IF NIP THEN
  ;

\ >NUMBER : u addr1 ul -- u addr2 ul
: >NUMBER
  BEGIN
    DUP IF OVER C@ >DIGIT ELSE FALSE THEN
  WHILE
    UNROT
    >R >R >R
    [ 10 [LITERAL] ] U*
    R> +
    R> CHAR+
    R> 1-
  REPEAT ;

\ ?UNUMBER : caddr -- n true | false
: ?UNUMBER
  0 SWAP
  COUNT
  >NUMBER
  \ u caddr ul --
  NIP
  \ remaining length should be 0
  DUP IF NIP THEN
  0=
  ;


: NEWLINE [ 10 [LITERAL] ] ;

\ ACCEPT : addr +n1 -- +n2
: ACCEPT
  0 BEGIN
    \ addr n1 n2 --
    2DUP >
    DUP IF DROP 
    [
      \ GET-KEY 
      0 COMPILE, 2 COMPILE, ]
    THEN
    DUP NEWLINE <> AND
    ?DUP
  WHILE
    \ addr n1 n2 c --
    >R
    ROT
    \ n1 n2 addr --
      R> OVER C! CHAR+ 
    \ n1 n2 addr+1 --
    UNROT 
    \ addr+1 n1 n2 --
    1+
  REPEAT
  NIP NIP
  ;

\ REFILL : -- flag
: REFILL
  HERE [ 1024 [LITERAL] ] + [ _TIB [LITERAL] ] !
  [ _TIB [LITERAL] ] @ [ 1024 [LITERAL] ] ACCEPT
  \ +n2 --
  [ _TIB [LITERAL] ] @ + 0 SWAP C!
  [
    \ EOF 
    0 COMPILE, 4 COMPILE, ]
  INVERT
  ;

\ The base interpreter. It only can work with decimal numbers,
\ and will display a single question mark when an unknown word
\ is encountered, but is otherwise fully functional.
: INTERPRET
  BEGIN
    BL WORD
    ?DUP
  WHILE
    DUP FIND
    ?DUP
    IF
      NIP 
      0< STATE C@ AND
      IF   COMPILE,
      ELSE EXECUTE
      THEN
    ELSE
      DUP ?UNUMBER
      IF
        NIP
        STATE C@
        IF [COMPILE] LITERAL
        THEN
      ELSE
        DROP
        [CHAR] ? 
        [
          \ EMIT
          0 COMPILE, 3 COMPILE, ]
      THEN
    THEN
  REPEAT ;

: QUIT
  [COMPILE] [
  BEGIN
    REFILL
  WHILE
    0 [ >IN [LITERAL] ] !
    INTERPRET
  REPEAT ;

:NONAME
  [ 
    \ SP!
    0 COMPILE, 1 COMPILE,
    \ RP !
    0 COMPILE, -1 COMPILE, ] 
  QUIT 
  [ 
    \ BYE
    0 COMPILE, 0 COMPILE, ]
  ;
_ABORT !

: :opcode CREATE COMPILE, POSTPONE EXIT IMMEDIATE
          DOES> STATE C@ IF @ COMPILE, ELSE EXECUTE THEN ;

