#!/usr/bin/perl
# code.pl
# (c) 2003,2004 Stephane Alnet
#
use strict; use warnings;

require 'params.pl';

my $char_chars;
# 'C4'; # Four bytes
# 'S2'; # Two 16-bits (untested)
$char_chars = a_char();

my $cell_char;
# 'V'; # 32-bit little-endian (i386)
# 'N'; # 32-bit big-endian (powerpc)
$cell_char = endian();

my @r = ();
while(<>)
{
  next if /^#/;
  chomp;
  s/\s*#.*$//;
  if( /^([0-9a-f]+) cell ([0-9a-f]+)$/io )
  {
    my @v = unpack($char_chars,pack($cell_char,hex($2)));
    my $a = hex($1);
    for my $b (0..one_cell()-1)
    {
      $r[$a+$b] = $v[$b];
    }
    next;
  }

  if( /^([0-9a-f]+) char ([0-9a-f]+)$/io )
  {
    $r[hex($1)] = hex($2);
    next;
  }

  die "Failed Line $.: '$_'\n";
}

# Zeroes undefined bytes
@r = map( $_ || 0, @r );

print pack('C*',@r);

