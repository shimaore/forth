#!/usr/bin/perl
# compile.pl - Forth Perl compiler
# (c) 2003,2004 Stephane Alnet
#

use strict; use warnings; use integer;

$| = 1;
our $debug = 0;

require "params.pl";

my $one_cell = one_cell();

my %opcode;

$opcode{'op_min'}   = 0;
$opcode{'imm'}      = imm();
$opcode{'neg_mask'} = neg_mask();
$opcode{'true'}     = true();
$opcode{'amask'}    = amask();
$opcode{'sysmask'}  = sysmask();

my %char = ();
my %cell = ();
my %word_nfa = ();
my %word_pfa = ();
my %word_imm = ();
my %word_run = ();
my $last_word = '';

sub here { 
  die unless exists $opcode{_DP}; 
  my $dp = $opcode{_DP}; 
  $cell{$dp} = $_[0], printf("%08x cell %08x  # DP\n",$dp,$cell{$dp}) if @_; 
  return $cell{$dp}; 
}

sub latest { 
  die unless exists $opcode{_LATEST}; 
  my $latest = $opcode{_LATEST}; 
  $cell{$latest} = $_[0], printf("%08x cell %08x  # LATEST\n",$latest,$cell{$latest}) if @_; 
  return $cell{$latest}; 
}

sub state { 
  die unless exists $opcode{_STATE}; 
  my $state = $opcode{_STATE}; 
  $char{$state} = $_[0], printf("%08x char %04x  # STATE\n",$state,$char{$state}) if @_; 
  return $char{$state}; 
}

sub tib { 
  die unless exists $opcode{_TIB}; 
  my $tib = $opcode{_TIB}; 
  $cell{$tib} = $_[0], printf("%08x cell %08x  # TIB\n",$tib,$cell{$tib}) if @_; 
  return $cell{$tib}; 
}

sub execute($)
{
  my $v = shift;
  my $pfa = $word_pfa{$v};
  printf "#: Executing %s at %08x\n", $v, $pfa if $debug;
  run_at($pfa);
  printf "#: Executed %s at %08x\n", $v, $pfa if $debug;
} 

sub char($)
{
  my $v = eval(shift);
  #
    printf "%08x char %04x  # %s\n", here(), $v, ($v >= 0x20 && $v <= 0x7f ? chr($v) : $v);
    $char{here()} = $v;
  #
  here( here() + 1 );
}

sub align($)
{
  my $a = shift;
  return $a unless $a % $one_cell;
  return $one_cell*(1+($a/$one_cell));
}

sub find($)
{
  my $w = shift;
  print "#. Find $w\n" if $debug;
  my $latest = latest();
  while($latest)
  {
    my $lfa = align($latest+1+($char{$latest}&sysmask()));
    printf "#.. Trying at %08x (LFA at %08x): ", $latest, $lfa if $debug;

    # print("wrong length\n"),
    $latest = $cell{$lfa}, next 
      unless ($char{$latest}&sysmask()) == length($w);
    my $f = 0;
    for my $i (0 .. length($w)-1)
    {
      $f += 1 if $char{$latest+1+$i} == ord(substr($w,$i,1));
    }

    # print("wrong word ($f)\n"),
    $latest = $cell{$lfa}, next unless $f == length($w);

    my $imm = $char{$latest}&imm();
    my $pfa = $lfa + $one_cell;
    $word_imm{$w} = $imm;
    $word_pfa{$w} = $pfa;
    printf "found %s at NFA=%08x, PFA=%08x\n", $w, $latest, $pfa if $debug;
    return 1;
  }
  print "#.. Could not find $w\n" if $debug;
  return 0;
}

sub cell($)
{
  my $v = shift;
  die unless defined $v;
  execute($v), return if $word_imm{$v};

  my $av = $v;
  $v = $opcode{$v} if exists $opcode{$v};

  $v = $opcode{$1}       if $v =~ /^_s:(.+)$/ && exists $opcode{$1};
  $v = eval($1) & true() if $v =~ /^_n:(-?\d+)$/;

  unless( $v =~ /^-?\d+$/ )
  {
    my $find = find($v);
    die("Non-integer opcode $v [line $.]\n"), return unless $find;
    execute($v), return if $word_imm{$v};
    $v = $word_pfa{$v};  
  }
  here( align( here() ) );
  #
    die "Hmm.. suspicious raw integer $v; replace with [ $v [LITERAL] ] at line $." if $v eq $av;
    printf "%08x cell %08x  # %s\n", here(), $v, ($v eq $av ? '' : $av);
    $cell{here()} = $v;
  #
  here( here() + $one_cell );
}

sub word($)
{
  my $v = shift;
  my $old_latest = latest()||0;
  latest( here() );
  char(length($v));
  for my $c (map( ord, split(//,$v)))
  { char($c); }
  print "#: LFA\n" if $debug;
  cell("_n:$old_latest");
  print "#: PFA\n" if $debug;
    $word_nfa{$v} = latest();
    $word_pfa{$v} = here();
    $word_imm{$v} = 0;
    $last_word = $v;
  return here();
}

sub immediate()
{
  $char{$word_nfa{$last_word}} |= imm();
  $word_imm{$last_word} = 1;
  print "#: $last_word is IMMEDIATE\n" if $debug;
    printf "%08x char %04x\n", $word_nfa{$last_word}, $char{$word_nfa{$last_word}};
}

sub execute_line($); 
sub compile_line($);
    

our %runcode = ();

sub main()
{
while(<>)
{
  chomp;
  my $op_min;

  print "# $_\n" if $debug;
  next if /^\s*$/;
  next if /\( ignore \)/;
  $debug++, next if /^\\ debug/;
  next if /^\s*\\ /;
  last if /^\\ STOP HERE/;

  print("#: Opcode $1 = $2 * one_cell\n"),
  $opcode{$1} = $2 * $one_cell,
  # Automatically compute OP_MIN
  $op_min = ($2+1) * $one_cell,
  $op_min &= true(),
  $opcode{'op_min'} = $opcode{'op_min'} > $op_min ? $opcode{'op_min'} : $op_min,
  next
    if /^: (\S+)\s*(\d+) \+ORIGIN ;$/;

  next if /\+ORIGIN/;

  $a = $1 & true(),
  print("#: Opcode $2 = $a\n"),
  $opcode{$2} = $a,
  $word_run{$2} = $a,
  $runcode{$a} = $runcode{$2},
  # Automatically compute OP_MIN
  $op_min = $a+1,
  $op_min &= true(),
  # Align
  $op_min = $op_min % $one_cell ? ($op_min/$one_cell+1)*$one_cell : $op_min,
  $opcode{'op_min'} = $opcode{'op_min'} > $op_min ? $opcode{'op_min'} : $op_min,
  next
    if /^\s*(-?\d+) :opcode (\S+)$/;

  s/^:\s+(\S+)\s*/_d:$1 /g;
  s/\s\['\]\s+(\S+)/ LIT _s:$1 /g;
  s/\s\[COMPILE\]\s+(\S+)/ _s:$1 /g;
  s/\s\[CHAR\]\s+(\S+)/" LIT _n:".ord($1)." "/ge;
  s/\sPOSTPONE\s+(\S+)/$word_imm{$1} ? " _s:$1 " : " LIT _s:$1 COMPILE, "/ge;
  s/\s\[\s+(-?\d+)\s+\[LITERAL\]\s+\]\s/ LIT _n:$1 /g;
  s/\s\[\s+(\S+)\s+\[LITERAL\]\s+\]\s/ LIT _s:$1 /g;
  print "#> $_\n" if $debug;

  $opcode{$1} = word($1),
  state(imm()),
  compile_line("$2 EXIT"),
  state(0),
  next
    if /^_d:(\S+)\s+(.*?)\s*;\s*$/;

  $opcode{$1} = word($1),
  state(imm()),
  compile_line("$2 EXIT"), 
  state(0),
  immediate(),
  next
    if /^_d:(\S+)\s+(.*?)\s*;\s+IMMEDIATE\s*$/;

  $opcode{$1} = word($1),
  state(imm()),
  next
    if /^_d:(\S+)\s*$/;

  $opcode{$1} = word($1),
  state(imm()),
  compile_line($2), 
  next
    if /^_d:(\S+)\s+(.*?)\s*$/;

  compile_line("$1 EXIT"), 
  state(0),
  next
    if state() && /^\s*(.*?)\s*;\s*$/;

  compile_line("$1 EXIT"), 
  state(0),
  immediate(),
  next
    if state() && /^\s*(.*?)\s*;\s+IMMEDIATE\s*$/;

  compile_line($_), next
    if state();

  die("#! Ignoring: $_ [line $.]\n"),
    if /^\s*\\/;

  execute_line($_);
}
} # main

use integer;
our ($ip,@sp,@rp);
%runcode = (
 'EXIT' => sub { $ip = pop(@rp) || 0; },
 'DROP' => sub { $#sp-- },
 'DUP'  => sub { push(@sp,$sp[-1]) },
 'OVER' => sub { push(@sp,$sp[-2]) },
 'SWAP' => sub { ($sp[-1],$sp[-2]) = ($sp[-2],$sp[-1]) },
 '@'    => sub { die unless exists $cell{$sp[-1]}; $sp[-1] = $cell{$sp[-1]} },
 '!'    => sub { die '!' unless $#sp >= 1;
       $cell{$sp[-1]} = $sp[-2]; 
       printf "%08x cell %08x  # %s\n", $sp[-1], $sp[-2], $sp[-2];
       $#sp -= 2 },
 'C@'   => sub { die 'C@' unless exists $char{$sp[-1]}; $sp[-1] = $char{$sp[-1]} },
 'C!'   => sub { $char{$sp[-1]} = $sp[-2]; 
       printf "%08x char %04x  # %s\n", $sp[-1], $sp[-2], ($sp[-2] >= 0x20 && $sp[-2] <= 0x7f)?chr($sp[-2]):$sp[-2];
       $#sp -= 2 },
 'R>' => sub { push(@sp,pop(@rp)) },
 '>R' => sub { push(@rp,pop(@sp)) },
 'AND' => sub { $sp[-2] &= $sp[-1]; $#sp-- },
 'OR'  => sub { $sp[-2] |= $sp[-1]; $#sp-- },
 'XOR' => sub { $sp[-2] ^= $sp[-1]; $#sp-- },
 '+C'  => sub { my $c = $sp[-1] + $sp[-2]; $c &= true(); 
       $sp[-1] = (($c < $sp[-1]) || ($c < $sp[-2])) ? 1 : 0;
       $sp[-2] = $c; },
 '2/' => sub { $sp[-1] /= 2; },
 '0=' => sub { $sp[-1] = ($sp[-1] == 0) ? true() : 0 },
 'U<' => sub { $sp[-2] = ($sp[-2] < $sp[-1]) ? true() : 0; $#sp-- },
 'CELL+' => sub { $sp[-1] += $one_cell; $sp[-1] &= true(); },
 'CELLS' => sub { $sp[-1] *= $one_cell; $sp[-1] &= true(); },
 'EMIT' => sub { print STDERR chr(pop(@sp)); },
 'LIT' => sub { die unless exists $cell{$ip}; push( @sp, $cell{$ip} ); $ip += $one_cell; },
 'BRANCH' => sub { die unless exists $cell{$ip}; $ip = $cell{$ip}; $ip &= true(); },
 '0BRANCH' => sub { die unless exists $cell{$ip}; $ip = pop(@sp) == 0 ? $cell{$ip} : $ip+$one_cell; $ip &= true(); },
 'R@' => sub { push( @sp, $rp[-1] ); },
 '+' => sub { $sp[-2] += $sp[-1]; $sp[-2] &= true(); $#sp--; },
 '1+' => sub { $sp[-1] ++; $sp[-1] &= true(); },
 'NEGATE' => sub { $sp[-1] = - $sp[-1]; $sp[-1] &= true(); },
 'INVERT' => sub { $sp[-1] = ~ $sp[-1]; },
 'ALIGNMENT' => sub { $sp[-1] = -($sp[-1] | amask()) & ~amask(); },
 );

sub run_at($)
{
  $ip = shift;

  while($ip)
  {
    my $w = $cell{$ip};
    die(sprintf("Invalid IP %08x", $ip)) unless defined($w);
    printf "#@ ip = %08x  w = %08x  sp = %s rp = %s\n", $ip, $w, 
            join(',',map(defined($_)?sprintf("%08x",$_):'<undef>',@sp)),
            join(',',map(defined($_)?sprintf("%08x",$_):'<undef>',@rp)),
            if $debug;
    $ip += $one_cell;
    if(defined($runcode{$w}))
    {
      $runcode{$w}->();
    }
    else
    {
      push(@rp,$ip);
      $ip = $w;
    }
  }

} 

sub execute_word($)
{
  my $w = shift;
    print "#:> Executing Word $w\n" if $debug;
    printf "#@ sp = %s rp = %s\n",
            join(',',map(defined($_)?sprintf("%08x",$_):'<undef>',@sp)),
            join(',',map(defined($_)?sprintf("%08x",$_):'<undef>',@rp)),
            if $debug;
  execute($w), return if exists $word_pfa{$w};
  $runcode{$word_run{$w}}->(), return if exists $word_run{$w};
  push(@sp,$opcode{$w}), return if exists $opcode{$w};
  push(@sp,$w), return if $w =~ /^-?\d+$/;
  die "Unknown word $w [line $.]" unless find($w);
  execute($w);
}
  

sub run_line($$$)
{
  my $t = shift;
  my $op = shift;
  my $run = shift;
  print "#: ${op}ing Line $t\n" if $debug;
  my $tib = tib((here()||0)+1024);
  printf "#: Filling TIB starting at %08x\n", $tib if $debug;
  for my $i (0 .. length($t)) # Adds NUL at the end
  {
    $char{$tib+$i} = ord(substr($t,$i,1))||0;
  }
  my $_in = $opcode{'>IN'};
  $cell{$_in} = 0;
  while( $cell{$_in} < length($t) )
  {
    # enclose..
    while($cell{$_in} < length($t) &&
          $char{$tib+$cell{$_in}} == ord(' ')) { $cell{$_in}++ };
    my $w = '';
    while($cell{$_in} < length($t) &&
          $char{$tib+$cell{$_in}} != ord(' '))
      { $w .= chr($char{$tib+$cell{$_in}}); $cell{$_in}++ };
    # enclose..
    while($cell{$_in} < length($t) &&
          $char{$tib+$cell{$_in}} == ord(' ')) { $cell{$_in}++ };
    $run->($w);
  }
}

sub execute_line($)
{
  run_line(shift,'Execut',\&execute_word);
}

sub compile_line($)
{
  run_line(shift,'Compil',\&cell);
}


main();

open(OP_MIN,'> op_min.h') or die;
print OP_MIN <<OP_MIN;
#ifndef __op_min_h
#define __op_min_h

#define OP_MIN $opcode{op_min}

#endif /* __op_min_h */
OP_MIN

