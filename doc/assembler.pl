#!/usr/bin/perl

use strict; use warnings;

# %resolve contain forward references that will need to
# be resolved later. (all CELLS)
our %resolve;

# %macros contain macros (which must be completely defined
# before they are used).
our %macros;

# $code contains the code generated as a byte string.
our $code = '';
# Default CHAR per CELL ratio (can be changed in bootstrap source).
our $char_per_cell = 1;

sub perl_pack_char
{
  return 'S' if $char_per_cell == 2;
  return 'L' if $char_per_cell == 4;
  return 'Q' if $char_per_cell == 8;
  die "Unknown char-per-cell ratio ${char_per_cell}.";
}

our $exit_code = 0;

# append_bytes($string) -- (implicit) appends a string of chars
sub append_bytes($)
{
  my $w = shift;
  # ALIGN
  while( length($w) % $char_per_cell )
  { $w .= chr(0); }
  # append the string
  $code .= $w;
}

# append_cells(@integers) -- (implicit) appends a list of cells
sub append_cells
{
  $code .= pack(perl_pack_char().'*',@_);
}

# set_cell($address,$integer) -- (implicit) replaces the content of address with the new value
sub set_cell($$)
{
  my $addr = shift;
  substr($code,$addr,$char_per_cell) = pack(perl_pack_char(),@_);
}

# returns the current position (HERE)
sub current
{ 
  return length($code); 
}

# set_current($address) -- .org
sub set_current($)
{
  my $l = shift;
  # shorten $code if the new length is less than the current length
  $code = substr($code,0,$l)           if $l < length($code);
  # append zeroes if the new length is more than the current length
  $code .= chr(0) x ($l-length($code)) if $l > length($code);
}

# macro_expand($text) -- returns an integer array, corresponding to $text
# after macro expansion
sub macro_expand($)
{
  # split on blanks (spaces, tabs, etc.)
  my @words = split(' ',shift);
  my @result = ();
  for my $word ( @words )
  {
    # if the word is defined as a macro, append the macro expansion for the word
    # otherwise the word has to be an explicit decimal value.

    $exit_code = 1,
    print STDERR "Undefined word '$word'." 
      if $word !~ /^-?\d+$/ && ! exists $macros{$word};

    push( @result, exists $macros{$word} ? @{$macros{$word}} : ("$word") );
  }
  return @result;
}

# resolve($name) -- writes the value for name at the positions marked for 
# cross-references.
sub resolve($)
{
  my $w = shift;
  return unless exists $resolve{$w};
  # Note that %resolve macros can only be one CELL long.
  my $value = $macros{$w}->[0];
  # Write all forward references values.
  for my $addr ( @{$resolve{$w}} )
  {
    set_cell($addr, $value);
  }
  # Since the symbol has been defined, we will never have to
  # fill references anymore.
  delete $resolve{$w};
}

# define_label($name) -- create a cross-reference list for a label
sub define_label($)
{
  my $w = shift;
  if( exists $macros{$w} && defined $macros{$w} && @{$macros{$w}} )
  {
    print STDERR "Duplicate label '$w' (".join(',',@{$macros{$w}}).").\n";
    $exit_code = 1;
  }
  # The macro for a label points at the current location.
  $macros{$w} = [current()];
  # Resolve any previous forward reference to this symbol.
  resolve($w);
}

# word($name,$mask) -- append NFA and LFA; uses $mask to mark a word immediate
sub word($$)
{
  my $w = shift;
  my $mask = shift;
  my $latest = current();
  # NFA
  append_bytes(chr(length($w)).$w);
  # LFA + immediate marker
  # Note: First word in the dictionary gets special treatment.
  append_cells($macros{lfa}->[0]|$mask) if  exists $macros{lfa};
  append_cells(0|$mask)                 if !exists $macros{lfa};
  $macros{lfa} = [$latest];
  # Create a label
  define_label($w);
}

# Initialization: define 'x' as a macro for char x.
# (This allows to write (Assembler) LIT 'x' for (Forth) [CHAR] x ).

for my $i ( 33..126 )
{
  $macros{"'".chr($i)."'"} = [ $i ];
}


# We use =pod .. =cut to mark comment blocks.
# $pod is <>0 in a comment block.
my $pod = 0;

while(<>)
{
  chomp;
  # Backslash comments
  s/\\.*$//;
  # Parenthesized comments
  s/(^|\s)\(\s[^)]+\)//g;
  # Canonalize spaces
  s/^\s+//; s/\s+$//; s/\s+/ /g;
  # Skip empty lines (blank lines or comments-only)
  next if /^$/;

  # Handles multi-line comments
  $pod = 0, next if $pod  && /^\)\)/;
  $pod = 1, next if !$pod && /^\(\(/;
  next if $pod;

  printf STDERR "# %08x %s\n", current(), $_;

  # .cell <value>
  $char_per_cell = int($1), next if /^.cell (\d+)$/;

  # .org  <value>
  set_current($1), next          if /^.org (\d+)$/;

  # :imm  <name>
  word($1,1), next               if /^:imm (\S+)$/;

  # :     <name>
  word($1,0), next               if /^: (\S+)$/;
  
  # .inline <name> <defn>   macro definition
  $macros{$1} = [macro_expand($2)], next if /^.inline (\S+) (\S.*)$/;

  # <name>:                 label definition
  define_label($1), next         if /^(\S+):$/;

  # Parse the words on the line
  for my $w ( split )
  {
    # literal integer number
    append_cells(int($1)), next if $w =~ /^(-?\d+)$/;

    # word/macro expansion
    my @a;
    @a = macro_expand($w), append_cells(@a), next if exists $macros{$w};

    # if the word is unknown, assume it is a forward label.
    # 1. create a list of references for the word if none exists
    $resolve{$w} = [] unless exists $resolve{$w};
    # 2. add the current location as a  reference to be filled-in later
    $resolve{$w}->[$#{$resolve{$w}}+1] = current();
    # 3. reserve a CELL to store the value once it is explicited
    append_cells(0);
  }
}

# Prints out a list of missing forward-labels.
# (Includes mispelled or undefined words.)
for my $missing ( keys %resolve )
{
  print STDERR "Undefined label or word: $missing\n";
  $exit_code = 1;
}

# Export the code
binmode STDOUT;
print $code;

exit($exit_code);

__END__

=pod

 This macro-assembler builds a bytecode by translating words
 into list of integers (cells). Words can be defined by:

 .inline <word>  <content>         macro
 <label>:                          label
 : <wordlabel>                     wordlabel
 :imm <wordlabel>                  wordlabel

 Macros are expanded recursively (but all words in a macro
 must previously have been defined).

 Labels are assigned the value of the memory location at which
 they appear. Labels can be forward-referenced (the assembler
 will list used-but-undefined labels).

 Additionally to the above label semantics,
   : <wordlabel>
 adds the length and representation of the name of the label in the 
 code and links to the last defined wordlabel (effectively creating
 the NFA and LFA for a Forth word in the dictionary).

 The special definition
   :imm <wordlabel>
 works the same way but sets the immediate flag on the word.

 The following operations are defined (besides the implicit
 "append" operation implied by integers, macros substitutions,
 and "cross-reference" implied by using labels and wordlabels):

  .cell <2|4>          defines the size of a cell, in chars
  .org  <address>      sets the current address (the location
                       where the next cell or char will be 
                       appended)
  

 The assembler also understand =pod and =cut for comments.
 Forth-style backslash comments are also supported.

=cut

=pod

# %resolve is used for forward labels.
# $resolve{$name} = [ locations where $name was referenced ]
# Call resolve($name) after $name has been defined (as a label) to
# fill the locations.


# %macros is used for macros.
# $macros{$name} = [ series of integer codes representing $name ]

# $code is the memory representation of the forth code.

=cut

