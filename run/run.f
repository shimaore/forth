\ (c) 2004-2008 Stephane Alnet

\ We define #! to ignore the end of the line.
\ This way we can skip the first line of a script and
\ ignore it -- however the name of the forth executable
\ MUST be preceded by a space (otherwise we will try to
\ parse the first line).

: #! postpone \ ;

\ Example:
\ First line of a scrip reads:
\ #! /usr/local/bin/forth
