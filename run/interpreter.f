function banner
function prompt
function compile-prompt

: .version s" 0.22.0" ;
:noname cr ." MyForth v" .version type space
           ." - (c) 2003-2020, Stephane Alnet." cr ; to banner
:noname ." OK " ; to prompt
:noname ." > " ; to compile-prompt

: unknown-word count type ." ? " ;

\ [compile] and postpone are almost the same word.
\ [compile] will append (compile) the execution-time of the (immediate) word.
\ postpone will append (compile) the compilation-time of the (immediate) word.

: [compile]
  (find) ( -- cpfa pfa | caddr 0 )
  ?dup
  if
    nip      ( cpfa pfa -- pfa )
    compile, ( pfa -- )
  else
    unknown-word ( caddr -- )
  then
  ; immediate

: postpone
  (find) ( -- cpfa pfa | caddr 0 )
  ?dup
  if
    postpone literal    ( pfa )
    @ postpone literal  ( cpfa )
    postpone execute    ( will execute the cpfa with pfa on the stack )
  else
    unknown-word
  then
  ; immediate


: interpret
  begin
    bl word
    ?dup
  while
    find ( caddr -- cpfa pfa | caddr 0 )
    ?dup
    if
      state @
      if   swap @ execute
      else nip execute
      then
    else
      dup ?number
      if
        nip
        state @
        if [compile] literal
        then
      else
        unknown-word exit
      then
    then
  repeat ;

: quit
  [compile] [
  ?interactive if banner then
  begin
    ?interactive if state @ if compile-prompt else prompt then then
    refill
  while
    0 >in !
    interpret
  repeat
  ;

:noname
  sp!
  rp!
  quit
  bye ;
_abort !

function abort
_abort to abort
